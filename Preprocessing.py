import cv2
import numpy as np
from Functions import length_line

def nothing(x):
    pass

def HSV_segment(frame):
    result = False
    frame_copy = frame.copy()
    hsv = cv2.cvtColor(frame_copy, cv2.COLOR_BGR2HSV)

    cv2.namedWindow("Tracking", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)
    cv2.createTrackbar("LH", "Tracking", 0, 255, nothing)
    cv2.createTrackbar("LS", "Tracking", 0, 255, nothing)
    cv2.createTrackbar("LV", "Tracking", 0, 255, nothing)
    cv2.createTrackbar("UH", "Tracking", 255, 255, nothing)
    cv2.createTrackbar("US", "Tracking", 255, 255, nothing)
    cv2.createTrackbar("UV", "Tracking", 255, 255, nothing)
    cv2.namedWindow("mask", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)

    while True:
        l_h = cv2.getTrackbarPos("LH", "Tracking")
        l_s = cv2.getTrackbarPos("LS", "Tracking")
        l_v = cv2.getTrackbarPos("LV", "Tracking")

        u_h = cv2.getTrackbarPos("UH", "Tracking")
        u_s = cv2.getTrackbarPos("US", "Tracking")
        u_v = cv2.getTrackbarPos("UV", "Tracking")

        l_b = np.array([l_h, l_s, l_v])
        u_b = np.array([u_h, u_s, u_v])

        mask = cv2.inRange(hsv, l_b, u_b)
        cv2.imshow("mask", mask)
        key = cv2.waitKey(1)

        if key == 27 or key == ord('q'):
            cv2.destroyAllWindows()
            threshed_orig = mask.copy()
            result = True
            print('Segmented image')
            cv2.imwrite('Segmented image.png', mask)
            break
    return threshed_orig, result

def Thresh_segment(frame, value, show):
    if show:
        cv2.namedWindow('Thresholding', flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)
        cv2.createTrackbar('Invert', 'Thresholding', value, 1, (lambda a: None))

        while cv2.getWindowProperty('Thresholding', cv2.WND_PROP_VISIBLE) > 0:
            invert = cv2.getTrackbarPos('Invert', 'Thresholding')
            if invert:
                ret, threshed = cv2.threshold(frame, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
            else:
                ret, threshed = cv2.threshold(frame, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

            cv2.imshow('Thresholding', threshed)
            k = cv2.waitKey(1)

            if k == 27 or k == ord('q') or k == ord(' '):
                break

        cv2.destroyAllWindows()
    else:
        invert = value
        if invert:
            ret, threshed = cv2.threshold(frame, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
        else:
            ret, threshed = cv2.threshold(frame, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)


    return threshed, invert

def get_line(action, x, y, flags, param):
    global pt_one, pt_two, cal_line
    if not cal_line:
        if action == cv2.EVENT_LBUTTONDOWN:
            pt_one = (x, y)
            pt_two = (x+1, y+1)
        elif action == cv2.EVENT_MOUSEMOVE:
            if pt_one != []:
                pt_two = (x, y)
        elif action == cv2.EVENT_LBUTTONUP:
            pt_two = (x, y)
            cal_line = True

def calibration(frame, known):
    global pt_one, pt_two, cal_line
    cal_line = False
    pt_one = []
    pt_two = []
    conv_factor = 1

    while True:
        cv2.namedWindow("Calibration", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)
        cv2.setMouseCallback("Calibration", get_line)
        image = frame.copy()

        if pt_one != []:
            cv2.line(image, pt_one, pt_two, (255, 0, 255), 2)

        cv2.imshow("Calibration", image)
        k = cv2.waitKeyEx(1)

        if k == ord('q') or k == ord('\x1b'):
            cv2.destroyWindow("Calibration")
            if cal_line:
                conv_factor = float(known) / length_line(pt_one, pt_two)
            return conv_factor
        if k == ord('r'):
            pt_one = []
            pt_two = []
            conv_factor = 1
