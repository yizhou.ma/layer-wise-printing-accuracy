import cv2, math
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from Gcode.Gcode_handling import segmentgcode
import plotly.express as px
import Cropping
from scipy.ndimage import gaussian_filter



# grid function
def img_to_grid(img, row,col):
    # this function takes an image and slice it into grids of row x col
    # the outputs are a list of grid images, number of col, number of row
    img = cv2.flip(img, 0) # flip the image to match the grids in the gcode
    ww = [[i.min(), i.max()] for i in np.array_split(range(img.shape[0]),row)]
    hh = [[i.min(), i.max()] for i in np.array_split(range(img.shape[1]),col)]
    grid = [[img[j:jj,i:ii,:],[i,j,ii,jj]] for j,jj in ww for i,ii in hh]
    return grid

def img_to_grid2(img, grid_size):
    # this function takes an image and slice it into grids of row x col
    # the outputs are a list of grid images, number of col, number of row

    height, width = img.shape[0], img.shape[1]
    row = int(width)/grid_size
    col = int(width)/grid_size
    img = cv2.flip(img, 0) # flip the image to match the grids in the gcode
    ww = [[i.min(), i.max()] for i in np.array_split(range(img.shape[0]),row)]
    hh = [[i.min(), i.max()] for i in np.array_split(range(img.shape[1]),col)]
    grid = [[img[j:jj,i:ii,:],[i,j,ii,jj]] for j,jj in ww for i,ii in hh]
    return grid

def plot_grid(grid,row,col,h=7,w=7):
    # this function is used to plot the grided image.
    # the row and col MUST be the same as the img_to_grid function
    fig, ax = plt.subplots(nrows=row, ncols=col)
    [axi.set_axis_off() for axi in ax.ravel()]

    fig.set_figheight(h)
    fig.set_figwidth(w)
    c = 0
    for row in ax:
        for col in row:
            col.imshow(np.flip(grid[c][0],axis=-1))
            c+=1
    plt.show()

def cal_grid(grids, img):
    # this function calculates the overextrusion and completion indices
    # obtained from the 2D assessment
    # results are returned as a pd dataframe
    results = []

    # The filters
    filt_yellow_L = np.array([26, 102, 0])
    filt_yellow_H = np.array([38, 136, 255])

    filt_red_L = np.array([0, 118, 50])
    filt_red_H = np.array([33, 255, 78])

    filt_gray_L = np.array([0, 0, 50])
    filt_gray_H = np.array([254, 59, 78])

    filt_blue_L = np.array([0, 0, 135])
    filt_blue_H = np.array([255, 255, 222])


    for n, grid in enumerate(grids):
        hsv = cv2.cvtColor(grid[0], cv2.COLOR_BGR2HSV)
        mask_yellow = cv2.inRange(hsv, filt_yellow_L, filt_yellow_H)
        mask_red = cv2.inRange(hsv, filt_red_L, filt_red_H)
        mask_gray = cv2.inRange(hsv, filt_gray_L, filt_gray_H)
        mask_blue = cv2.inRange(hsv, filt_blue_L, filt_blue_H)

        red = cv2.countNonZero(mask_red)
        yellow = cv2.countNonZero(mask_yellow)
        gray = cv2.countNonZero(mask_gray)
        blue = cv2.countNonZero(mask_blue)

        completion = gray / (red + gray + 1)
        expansion = (yellow + gray + blue*0.3+1) / (red + gray + 1)

        results.append([n+1, completion, expansion, grid[1][0],grid[1][1], grid[1][2], grid[1][3]])
    results = pd.DataFrame(results, columns=['Grid', 'Completion', 'Expansion', 'X1', 'Y1', 'X2', 'Y2'])
    return results

def _gcode_to_grid(gcode, row, col):
    # this function slices gcode into grids of row x col
    # the output is a pd dataframe with a grid column
    coord, coordDF, bound = segmentgcode(gcode)

    xmin, xmax = round(coordDF["X"].min()), round(coordDF["X"].max())
    ymin, ymax = round(coordDF["Y"].min()), round(coordDF["Y"].max())
    xseg = [(i.min()-1, i.max()+2) for i in np.array_split(range(xmin, xmax),col)]
    yseg = [(i.min()-1, i.max()+2) for i in np.array_split(range(ymin, ymax),row)]

    n = 1
    coordDF = coordDF.reset_index()
    coordDF['Grid'] = ' '
    for ys in yseg:
        for xs in xseg:
            idx = coordDF.index[coordDF["X"].between(xs[0], xs[1]) & coordDF["Y"].between(ys[0], ys[1])]
            coordDF.loc[idx, 'Grid'] = n
            n = n+1
    return coordDF, bound

def plot_gcode_grid(df):
    # this is a quick plotly function to visualize the grided gcode
    # it MUST take a pd dataframe from the output of gcode_to_grid
    fig = px.line(
        df,
        x="X",
        y="Y",
        color="Grid"

    )

    fig["layout"].pop("updatemenus") # optional, drop animation buttons
    fig.show()

def cal_gcode_grid(gcode, row, col, plot = False, shift =[0,0]):
    df, bound = _gcode_to_grid(gcode, row, col)
    crop_mm = 3
    if plot:
        plot_gcode_grid(df)
    moves = df.groupby(["Grid"])["Grid"].count().to_list()
    ssp = df.groupby(['Grid']).apply(lambda x: (x['index']==0).sum()).to_list()
    df['X_diff'] = df['X'].diff()
    df['Y_diff'] = df['Y'].diff()
    bound_pix = [int(i / 0.1) for i in bound]
    crop_margin = int(crop_mm / 0.1)
    df['X_pix'] = np.floor(((df['X']-50)/0.1 - shift[0]) - (bound_pix[0]-int(50/0.1)-crop_margin))
    df['Y_pix'] = np.floor((df['Y']/0.1 + shift[1]) - bound_pix[1]-crop_margin)
    df['Distance'] = np.sqrt(df['X_diff'] ** 2 + df['Y_diff'] ** 2)
    df['Angle'] = np.arctan(df['Y_diff'] / df['X_diff']) * 180 / np.pi
    df['ssp'] = 0
    df['ssp'][df['index'] == 0] = 1
    results = df.groupby('Grid')['Distance', 'Angle'].mean()
    results['moves'] = moves
    results['ssp'] = ssp
    results = results.reset_index()
    return results, df

def cal_gcode_pairs(gcode, shift=[0,0]):
    _, df = cal_gcode_grid(gcode, 1, 1, shift=shift)
    df = df.drop(['X_diff', 'Y_diff', 'Grid'], 1)
    x_list = df['X'].drop(labels=0, axis=0).to_list()
    x_list.append(np.NaN)
    y_list = df['Y'].drop(labels=0, axis=0).to_list()
    y_list.append(np.NaN)
    xpix_list = df['X_pix'].drop(labels=0, axis=0).to_list()
    xpix_list.append(np.NaN)
    ypix_list = df['Y_pix'].drop(labels=0, axis=0).to_list()
    ypix_list.append(np.NaN)
    Dis_list = df['Distance'].drop(labels=0, axis=0).to_list()
    Dis_list.append(np.NaN)
    Angle_list = df['Angle'].drop(labels=0, axis=0).to_list()
    Angle_list.append(np.NaN)
    df['X2'] = x_list
    df['Y2'] = y_list
    df['X_pix2'] = xpix_list
    df['Y_pix2'] = ypix_list
    df['Angle'] = Angle_list
    df['Distance'] = Dis_list

    df = df.drop(labels=(len(df.index)-1), axis=0)
    df = df[['index', 'X', 'Y', 'X2', 'Y2', 'Z',
             'X_pix', 'Y_pix', 'X_pix2', 'Y_pix2',
             'Step', 'Distance', 'Angle', 'ssp']]
    df['Non-printing'] = 0
    index = df.index[df['ssp'] == 1]-1
    index = index.to_list()
    index.pop(0)
    df.loc[index, 'Non-printing'] = 1
    return df

def find_img_infill(img):
    #the img is the raw image file from the browse image
    cropped_img, topleft, bottomright = Cropping.crop_selection(img)
    cropped_img = cv2.GaussianBlur(cropped_img, (7, 7), 1)
    cropped_img = cv2.cvtColor(cropped_img, cv2.COLOR_BGR2GRAY)
    ret, threshed = cv2.threshold(cropped_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, hierarchy = cv2.findContours(image=threshed, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
    if contours:
        c = max(contours, key=cv2.contourArea)
        area_total = cv2.contourArea(c)
        area_print = np.sum(threshed == 255)
        infill = np.round(area_print/area_total, 3)
    else:
        infill = 0
    return infill

def find_gcode_infill(gcode_mask):
    #the gcode_mask is the binary image output from the overlay Gcode
    cropped_img, topleft, bottomright = Cropping.crop_selection(gcode_mask)
    cropped_img = cv2.cvtColor(cropped_img, cv2.COLOR_BGR2GRAY)
    ret, threshed = cv2.threshold(cropped_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, hierarchy = cv2.findContours(image=threshed, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
    if contours:
        c = max(contours, key=cv2.contourArea)
        area_total = cv2.contourArea(c)
        area_print = np.sum(threshed == 255)
        infill = np.round(area_print / area_total, 3)
    else:
        infill = 0
    return infill


def grid_to_heatmap(data, img, quantile=99, over=True, show=True):
    # This is the visualization function to get the CV-measurement heatmap of expansion
    #data = grid results
    #img = cropped image
    gray_img_shape = img.shape[0], img.shape[1], 1
    heatmap = np.zeros(shape=gray_img_shape, dtype=np.uint8)
    maxint = np.nanpercentile(data['Expansion'], quantile)  # Using 99 percentile as the maximum number

    data.loc[data['Expansion'] > maxint, 'Expansion'] = maxint
    data['Expansion_normed'] = (data['Expansion'] - data['Expansion'].min()) / (data['Expansion'].max() - data['Expansion'].min()) *255
    data['Expansion_normed'] = data['Expansion_normed'].fillna(data['Expansion_normed'].max())
    data['Completion_normed'] = (data['Completion'] - data['Completion'].min()) / (data['Completion'].max() - data['Completion'].min()) *255
    data['Completion_normed'] = data['Completion_normed'].fillna(data['Completion_normed'].max())


    if over:
        cols = ['X1', 'X2', 'Y1', 'Y2', 'Expansion_normed']

        list = data[cols].values.tolist()
        for box in list:
            heatmap[int(box[2]):int(box[3]), int(box[0]):int(box[1])] = int(box[4])
        smoothed_heatmap = gaussian_filter(heatmap, sigma=5)
        heatmapshow = cv2.applyColorMap(smoothed_heatmap, cv2.COLORMAP_JET)
        heatmapshow = cv2.flip(heatmapshow, 0)
        overlay = cv2.addWeighted(heatmapshow, 0.5, img, 0.5, 0)
        gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        ret, threshed = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        contours, hierarchy = cv2.findContours(image=threshed, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
        if contours:
            c = max(contours, key=cv2.contourArea)
        mask = np.zeros(shape=img.shape, dtype=np.uint8)
        cv2.drawContours(mask, [c], -1, color=(255, 255, 255), thickness=-1)
        result = cv2.bitwise_and(overlay, mask)
        if show:
            cv2.namedWindow('Over Extrusion Heatmap', flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)
            cv2.imshow("Over Extrusion Heatmap", result)
            cv2.waitKey(0)
            cv2.destroyAllWindows()

    return result, smoothed_heatmap

def visualize_under(crop_img, assessed_img, show=True):
    hsv = cv2.cvtColor(assessed_img, cv2.COLOR_BGR2HSV)

    filt_red_L = np.array([0, 118, 50])
    filt_red_H = np.array([33, 255, 78])
    mask_red = cv2.inRange(hsv, filt_red_L, filt_red_H)
    missing_color = np.zeros(shape=crop_img.shape, dtype=np.uint8)
    missing_color[mask_red>0] = (0,230,254)
    missing_on_print = cv2.addWeighted(missing_color, 1.2, crop_img, 0.5, 0)
    gray_img = cv2.cvtColor(missing_on_print, cv2.COLOR_BGR2GRAY)
    ret, threshed = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, hierarchy = cv2.findContours(image=threshed, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
    if contours:
        c = max(contours, key=cv2.contourArea)
    mask = np.zeros(shape=missing_on_print.shape, dtype=np.uint8)
    cv2.drawContours(mask, [c], -1, color=(255, 255, 255), thickness=-1)
    result = cv2.bitwise_and(missing_on_print, mask)
    if show:
        cv2.namedWindow("results", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO) # dragable windows

        cv2.imshow("results", result)
        cv2.waitKey(0)
    return result

def _survey_to_heatmap(img, survey_map, win_size = 3, scale=1, rotate=True):
    # This is the OLD method to visualize consumer survey on the heatmap.
    # img = cropped survey image
    # survey_map = coordinates of the survey clicks
    # scale = shrinking scale used for deer images
    # rotate = 90 degree rotation of the block cookies
    img_shape = [img.shape[0], img.shape[1], 1]
    heatmap = np.zeros(shape=img_shape, dtype=np.uint8)
    to_plot_map = survey_map.dropna()
    to_plot_list = to_plot_map.to_numpy().tolist()
    for line in to_plot_list:
        x = int(line[2] * scale)
        y = int(line[3] * scale)
        heatmap[(y - win_size):(y + win_size), (x - win_size):(x + win_size)] = 255

    smoothed_heatmap = gaussian_filter(heatmap, sigma=4)
    heatmapshow = cv2.applyColorMap(smoothed_heatmap, cv2.COLORMAP_JET)
    overlay = cv2.addWeighted(heatmapshow, 0.5, img, 0.5, 0)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, threshed = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, hierarchy = cv2.findContours(image=threshed, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
    if contours:
        c = max(contours, key=cv2.contourArea)

    mask = np.zeros(shape=img.shape, dtype=np.uint8)
    cv2.drawContours(mask, [c], -1, color=(255, 255, 255), thickness=-1)
    result = cv2.bitwise_and(overlay, mask)
    cv2.namedWindow('survey_heatmap1', flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)

    if rotate:
        result = cv2.rotate(result, cv2.ROTATE_90_CLOCKWISE)

    cv2.imshow('survey_heatmap1', result)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return result


def survey_to_heatmap2(img, survey_map, grid_size=15, click_thresh=3, scale=1, rotate=True, show=True):
    # This is the current method used to generate consumer survey heatmap. It is slow, but less agressive.
    # img = cropped survey image
    # survey_map = coordinates of the survey clicks
    # scale = shrinking scale used for deer images
    # rotate = 90 degree rotation of the block cookies
    img_shape = [img.shape[0], img.shape[1], 3]
    heatmap = np.zeros(shape=img_shape, dtype=np.uint8)

    grids = img_to_grid2(heatmap, grid_size)

    for grid in grids:
        box = grid[1]
        df = survey_map[(survey_map.x > box[0]/scale) &
                        (survey_map.x < box[2]/scale) &
                        (survey_map.y > box[1]/scale) &
                        (survey_map.y < box[3]/scale)]
        grid.append(df.shape[0])
        if df.shape[0] >=click_thresh:
            heatmap[int(box[1]):int(box[3]), int(box[0]):int(box[2])] = int(df.shape[0])*255
        else:
            heatmap[int(box[1]):int(box[3]), int(box[0]):int(box[2])] = 0


    smoothed_heatmap = gaussian_filter(heatmap, sigma=10)
    smoothed_heatmap = cv2.cvtColor(smoothed_heatmap, cv2.COLOR_BGR2GRAY)
    heatmapshow = cv2.applyColorMap(smoothed_heatmap, cv2.COLORMAP_JET)
    overlay = cv2.addWeighted(heatmapshow, 0.5, img, 0.5, 0)
    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, threshed = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, hierarchy = cv2.findContours(image=threshed, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
    if contours:
        c = max(contours, key=cv2.contourArea)
    mask = np.zeros(shape=img.shape, dtype=np.uint8)
    cv2.drawContours(mask, [c], -1, color=(255, 255, 255), thickness=-1)
    result = cv2.bitwise_and(overlay, mask)
    if rotate:
        result = cv2.rotate(result, cv2.ROTATE_90_CLOCKWISE)
    if show:
        cv2.namedWindow('Survey_heatmap2', flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)

        cv2.imshow('Survey_heatmap2', result)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    return result, smoothed_heatmap

def match_cropped_heatmap(template_color, raw_color, survey_map, grid_size=15, scale=1, click_thresh=2):
    # This function plots the survey data on to a heatmap of the raw wrapped image. Very slow...
    # template_color = cropped image of the printed object
    # raw_color = wrapped image
    # survey_map = survey clicks data from the R pipeline
    template = cv2.cvtColor(template_color, cv2.COLOR_BGR2GRAY)
    raw_color = cv2.flip(raw_color, 0)
    raw_color = cv2.rotate(raw_color, cv2.ROTATE_90_COUNTERCLOCKWISE)
    raw = cv2.cvtColor(raw_color, cv2.COLOR_BGR2GRAY)
    w, h, = template.shape[::-1]
    res = cv2.matchTemplate(raw, template, cv2.TM_CCOEFF)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = max_loc
    heatmap_vis = np.zeros(shape=raw_color.shape, dtype=np.uint8)
    heatmap_raw = np.zeros(shape=raw_color.shape, dtype=np.uint8)

    grids = img_to_grid2(heatmap_vis, grid_size)

    for grid in grids:
        box = grid[1]
        df = survey_map[(survey_map.x+top_left[0] > box[0]/scale) &
                        (survey_map.x+top_left[0] < box[2]/scale) &
                        (survey_map.y+top_left[1] > box[1]/scale) &
                        (survey_map.y+top_left[1] < box[3]/scale)]
        grid.append(df.shape[0])
        if df.shape[0] >=click_thresh:
            heatmap_vis[int(box[1]):int(box[3]), int(box[0]):int(box[2])] = int(df.shape[0])*100
            heatmap_raw[int(box[1]):int(box[3]), int(box[0]):int(box[2])] = int(df.shape[0])
        else:
            heatmap_vis[int(box[1]):int(box[3]), int(box[0]):int(box[2])] = 0
            heatmap_raw[int(box[1]):int(box[3]), int(box[0]):int(box[2])] = 0

    smoothed_heatmap = gaussian_filter(heatmap_vis, sigma=10)
    smoothed_heatmap = cv2.cvtColor(smoothed_heatmap, cv2.COLOR_BGR2GRAY)
    heatmapshow = cv2.applyColorMap(smoothed_heatmap, cv2.COLORMAP_JET)
    overlay = cv2.addWeighted(heatmapshow, 0.5, raw_color, 0.5, 0)
    overlay = cv2.rotate(overlay, cv2.ROTATE_90_CLOCKWISE)
    cv2.namedWindow("test", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO) # dragable windows
    cv2.imshow('test', overlay)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    heatmap_raw = cv2.rotate(heatmap_raw, cv2.ROTATE_90_CLOCKWISE)
    return heatmap_raw

def match_heatmap(template_color, raw_color, survey_map):
    # This function produces the new x and y coordinates for the clicks on the wrapped image
    # template_color = cropped image of the printed object
    # raw_color = wrapped image
    # survey_map = survey clicks data from the R pipeline
    template = cv2.cvtColor(template_color, cv2.COLOR_BGR2GRAY)
    raw_color = cv2.flip(raw_color, 0)
    raw_color = cv2.rotate(raw_color, cv2.ROTATE_90_COUNTERCLOCKWISE)
    raw = cv2.cvtColor(raw_color, cv2.COLOR_BGR2GRAY)
    w, h, = template.shape[::-1]
    res = cv2.matchTemplate(raw, template, cv2.TM_CCOEFF)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = max_loc
    survey_map['matched_x'] = survey_map['x']+top_left[0]
    survey_map['matched_y'] = survey_map['y']+top_left[1]
    return survey_map



def draw_gcode_survey(coord, img, survey_map, linewidth, coordBound,
                      pixpermm, ssp=True, draw_clicks=True, line=True):
    gcode_img = np.zeros(shape=img.shape, dtype=np.uint8)
    survey_map = survey_map.dropna()
    survey_map_list = survey_map.values.tolist()
    width = int(linewidth / pixpermm)
    if line:
        for segment in coord:  # plot lines per segment
            x = [item[0] for item in segment]
            y = [item[1] for item in segment]
            y_invert = [0 - n for n in y]
            for i in range(0, len(x) - 1):
                x1, y1 = int((x[i])/pixpermm), int(y_invert[i]/pixpermm+ gcode_img.shape[0])
                x2, y2 = int((x[i+1])/pixpermm), int(y_invert[i+1]/pixpermm+ gcode_img.shape[0])
                cv2.line(gcode_img, (x1, y1), (x2, y2), (140, 217, 245), width)
    else:
        for segment in coord:  # plot lines per segment
            x = [item[0] for item in segment]
            y = [item[1] for item in segment]
            y_invert = [0 - n for n in y]
            for i in range(0, len(x)):
                x_plot = int(x[i]/pixpermm)
                y_plot = int(y_invert[i]/pixpermm+ gcode_img.shape[0])
                cv2.circle(gcode_img, (x_plot, y_plot), 5, (140, 217, 245), -1)

    if ssp:
        for segment in coord:
            xpt1 = int(segment[0][0]/pixpermm)
            ypt1 = int((0 - segment[0][1])/pixpermm+gcode_img.shape[0])
            xpt2 = int(segment[-1][0]/pixpermm)
            ypt2 = int((0 - segment[-1][1])/pixpermm+gcode_img.shape[0])
            cv2.circle(gcode_img, (xpt1, ypt1), int(width*1.2), (180, 217, 245), -1)
            cv2.circle(gcode_img, (xpt2, ypt2), int(width*1.2), (180, 217, 245), -1)
    if draw_clicks:
        gcode_img = cv2.rotate(gcode_img, cv2.ROTATE_90_COUNTERCLOCKWISE)
        for click in survey_map_list:
            xclick = int(click[4])
            yclick = int(click[5])
            cv2.circle(gcode_img, (xclick, yclick), 5, (255, 255,255), -1)
        gcode_img = cv2.rotate(gcode_img, cv2.ROTATE_90_CLOCKWISE)

    bound_pix = [int(i / pixpermm) for i in coordBound]
    crop_mm = -5
    crop_margin = int(crop_mm / pixpermm)
    ymin = bound_pix[1] - crop_margin
    ymax = bound_pix[3] + crop_margin
    xmin = bound_pix[0] - crop_margin
    xmax = bound_pix[2] + crop_margin
    xmid = int((xmax-xmin)/2+xmin)
    gcode_img_cropped = gcode_img[ymin: ymax, xmin: xmid]
    cv2.namedWindow("test", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)  # dragable windows
    cv2.imshow('test', gcode_img_cropped)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

#heatmap = pd.read_csv('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Survey/Cleaned heatmap survey/deer_fresh_heatmap.csv')
#img = cv2.imread('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Cropped_images/For survey/shorten_deer_fresh_cropped.jpg')
#raw_img = cv2.imread('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Cropped_images/shorten_deer_fresh_wrapped.jpg')
#gcode = 'C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Cookie Gcodes/No_sk_Geometric deer head.gcode'
#coord, coordDf, coordBound = segmentgcode(gcode)
#survey_to_heatmap2(img, heatmap, grid_size=15, click_thresh=3, scale=2.2, rotate=False)
#match_cropped_heatmap(img, raw_img, heatmap, grid_size=15, scale=1, click_thresh=2)
#
#survey_map = match_heatmap(img, raw_img, heatmap)
#draw_gcode_survey(coord, raw_img,survey_map, linewidth=1.5, coordBound=coordBound,
#                      pixpermm=0.1, ssp=True, draw_clicks=True, line=False)