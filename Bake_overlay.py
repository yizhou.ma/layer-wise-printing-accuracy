import cv2
import numpy as np
from scipy import signal
from Video_analysis import overlay

import Cropping

def correct_shift_gcode2(bake, fresh, shift):
    num_rows, num_cols = fresh.shape[:2]
    translation_matrix = np.float32([[1, 0, -shift[0]], [0, 1, -shift[1]]])
    img_translation = cv2.warpAffine(fresh, translation_matrix, (num_cols, num_rows))
    shifted, image_assessed, to_save = overlay(bake, img_translation, show=True)


def run_baking_analysis():
    bake = cv2.imread('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/binary_images/Bo_deer_baked_binary.jpg', 0)
    fresh = cv2.imread('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/binary_images/Bo_deer_fresh_binary.jpg')
    shift, img, to_save = overlay(bake, fresh, show=False)
    frame, top_left, bottom_right = Cropping.crop_selection(img)
    crop_bake = bake[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
    crop_fresh = fresh[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
    shift, expansion, retention = overlay(crop_bake, crop_fresh, show=True)
    correct_shift_gcode2(crop_bake, crop_fresh, shift)
    #grid = img_to_grid(img, 20, 20)
    #results = cal_grid(grid)
    #plot_grid(grid, 20, 20)
    #print(results)
    #non_zeros = results['Expansion'] != 0
    #non_zero_results = results[non_zeros]
    #non_zero_results.plot.scatter(x='Expansion', y='Completion')
    #plt.show()

def match_cropped():
    template = cv2.imread('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Cropped_images/shorten_deer_baked_cropped.jpg', 0)
    w, h, = template.shape[::-1]
    raw = cv2.imread('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Cropped_images/shorten_deer_baked_wrapped.jpg', 0)
    raw = cv2.flip(raw, 0)
    res = cv2.matchTemplate(raw, template, cv2.TM_CCOEFF)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = max_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)
    print((top_left, bottom_right))
    cv2.rectangle(raw, top_left, bottom_right, 255, 2)
    cv2.namedWindow("test", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO) # dragable windows
    cv2.imshow('test', raw)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

match_cropped()





