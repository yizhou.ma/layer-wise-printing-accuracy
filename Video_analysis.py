import cv2
import numpy as np
from scipy import signal
from Gcode import Gcode_handling
import matplotlib.pyplot as plt
import math


points_cal = []

def select_points_calibration(action, x, y, flags, *userdata):
    global points_cal
    # Mark a dot, when left mouse button is pressed
    if action == cv2.EVENT_LBUTTONDOWN:
        points_cal.append([x, y])

def img_cam(img_file, g_specs, gcodetype, gcode_file, crop_mm, filament_width=1.5, show=True, shift_x=0, shift_y=0,
            wrap_factor = 10, ssp=False):
    filament_width = float(filament_width)

    if crop_mm == '-':
        crop_mm = '0'
    crop_mm = float(crop_mm)

    width = int(g_specs["width"])
    height = int(g_specs["height"])
    x_offset = int(g_specs["x_offset"])
    y_offset = int(g_specs["y_offset"])

    orig_frame = cv2.imread(img_file, 1)
    if gcodetype == 'gco' or gcodetype == 'gcode':
        coords, df, bound = Gcode_handling.segmentgcode(gcode_file, segby='G92') # Segment Gcode and find bounding box
        temp_z = []
        for segment in coords:  # plot lines per segment
            z = [item[2] for item in segment]
            temp_z.append(np.unique(z))
        layer_z = np.unique(temp_z)
        layer_height = layer_z[2] - layer_z[1]

    if gcodetype == 'csv':
        coords, df, bound = Gcode_handling.loadnordsoncoord(gcode_file) # Segment Gcode and find bounding box

    if show:
        cv2.namedWindow("FrameImg", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)

    frame = orig_frame.copy()
    img_blurred = cv2.blur(frame, (11,11)) # Blur to improve contour detection
    img_hsv = cv2.cvtColor(img_blurred, cv2.COLOR_BGR2HSV)
    # HSV filter to find the green calibration dots
    if gcodetype == 'gco' or gcodetype == 'gcode':
        lower = np.array([37, 44, 109])
        upper = np.array([62, 255, 255])
    if gcodetype == 'csv':
        lower = np.array([33, 77, 108])
        upper = np.array([78, 188, 255])
    mask = cv2.inRange(img_hsv, lower, upper)
    filtered_frame = cv2.bitwise_and(frame, frame, mask=mask)
    gray_frame = cv2.cvtColor(filtered_frame, cv2.COLOR_BGR2GRAY)

    gray_frame_blurred = cv2.blur(gray_frame, (3,3)) # Blur to improve contour detection
    edged = cv2.Canny(gray_frame_blurred, 30, 200)
    # Find contours based on the filtered dots
    contours, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    contours = sorted(contours, key=len, reverse=True)
    dot_centers = [] # Find the centers of the dot
    for c in contours[0:4]:
        M = cv2.moments(c)
        cX = int(M["m10"] / M["m00"])
        cY = int(M["m01"] / M["m00"])
        dot_centers.append([cX, cY])
    points = get_square2(dot_centers)
    # Crop and wrap perspective based on dot positions
    points_orig = np.float32([[0, 0], [width * wrap_factor, 0], [width * wrap_factor, height * wrap_factor], [0, height * wrap_factor]])
    points = np.float32(points)
    matrix = cv2.getPerspectiveTransform(points, points_orig)
    warped_output = cv2.warpPerspective(frame, matrix, (width*wrap_factor, height*wrap_factor))
    warped_output = cv2.flip(warped_output, 0) # flip image for G-code plotting
    overlayed_output = warped_output.copy()
    x_scale = width/np.shape(overlayed_output)[1] # find the scale of the transformed image
    output_gcode_mask = np.zeros(overlayed_output.shape, dtype=np.uint8) # Create an empty frame for G-code mask
    width_all = []
    if gcodetype == 'gco' or gcodetype == 'gcode':
        for segment in coords:  # Segment and plot the G-code
            x = [item[0] for item in segment]
            y = [item[1] for item in segment]
            e = [item[3] for item in segment]
            for i in range(0, len(x) - 1):
                dis = math.sqrt((x[i] - x[i + 1]) ** 2 + (y[i] - y[i + 1]) ** 2)
                #L = (e[i + 1] - e[i]) * (27 ** 2 / filament_width ** 2)
                #w1 = 3.14 * L * (filament_width ** 2) / (4 * layer_height * dis) + (1 - 3.14 / 4) * layer_height
                w2 = 3.14 * (27 ** 2) * (e[i + 1] - e[i]) / (4 * layer_height * dis) + (1 - 3.14 / 4) * layer_height
                width_all.append(round(w2, 3))

                l_width = int(round(w2 / x_scale))

                x1, y1 = int(x[i] / x_scale), int(y[i]/ x_scale)
                x2, y2 = int(x[i + 1] / x_scale), int(y[i + 1] / x_scale)
                x1 = x1 - shift_x
                x2 = x2 - shift_x
                y1 = y1 + shift_y
                y2 = y2 + shift_y

                if l_width != 0:
                    cv2.line(overlayed_output, (x1, y1), (x2, y2), (0, 0, 255), l_width)
                    cv2.line(output_gcode_mask, (x1, y1), (x2, y2), (255, 255, 255), l_width)
    width_mode = max(set(width_all), key=width_all.count)

    if ssp:
        overlayed_output = cv2.flip(overlayed_output, 0)
        output_gcode_mask = cv2.flip(output_gcode_mask, 0)
        dot_width = width_mode/x_scale
        for segment in coords:
            xpt1 = int(segment[0][0]/x_scale)
            ypt1 = int((0 - segment[0][1])/x_scale+overlayed_output.shape[0])
            xpt2 = int(segment[-1][0]/x_scale)
            ypt2 = int((0 - segment[-1][1])/x_scale+overlayed_output.shape[0])
            cv2.circle(overlayed_output, (xpt1, ypt1), int(dot_width*1.05), (0, 255, 255), -1)
            cv2.circle(overlayed_output, (xpt2, ypt2), int(dot_width*1.05), (0, 255, 255), -1)
            cv2.circle(output_gcode_mask, (xpt1, ypt1), int(dot_width * 1.05), (255, 255, 255), -1)
            cv2.circle(output_gcode_mask, (xpt2, ypt2), int(dot_width * 1.05), (255, 255, 255), -1)
        overlayed_output = cv2.flip(overlayed_output, 0)
        output_gcode_mask = cv2.flip(output_gcode_mask, 0)
    if gcodetype == 'csv':
        x_gcode = [item[0] - x_offset for item in coords]
        y_gcode = [item[1] - y_offset for item in coords]
        for i in range(0, len(x_gcode) - 1):
            x1, y1 = int(x_gcode[i] / x_scale), int(y_gcode[i] / x_scale)
            x2, y2 = int(x_gcode[i + 1] / x_scale), int(y_gcode[i + 1] / x_scale)
            x1 = x1 - shift_x
            x2 = x2 - shift_x
            y1 = y1 - shift_y
            y2 = y2 - shift_y
            l_width = int(filament_width / x_scale)
            cv2.line(overlayed_output, (x1, y1), (x2, y2), (0, 0, 255), l_width)  # Overlay G-code on warped image
            cv2.line(output_gcode_mask, (x1, y1), (x2, y2), (255, 255, 255), l_width)  # Create G-code mask


    # set up the auto-crop frame
    bound_pix = [int(i/x_scale) for i in bound]
    crop_margin = int(crop_mm/x_scale)
    ymin=bound_pix[1]-int(y_offset/x_scale)-crop_margin
    ymax=bound_pix[3]-int(y_offset/x_scale)+crop_margin
    xmin=bound_pix[0]-int(x_offset/x_scale)-crop_margin
    xmax=bound_pix[2]-int(x_offset/x_scale)+crop_margin
    output_gcode_mask = output_gcode_mask[ymin:ymax, xmin:xmax]
    output_gcode_mask = cv2.flip(output_gcode_mask, 0) # flip the mask, too
    clean_output = warped_output[ymin:ymax, xmin:xmax] # crop the clean output as specified by gcode size
    image_overlay = overlayed_output[ymin:ymax, xmin:xmax] # also crop the output with gcode overlay
    clean_output = cv2.flip(clean_output, 0) # flip the clean output, too
    image_overlay = cv2.flip(image_overlay, 0)
    width_mode = max(set(width_all), key=width_all.count)
    if show:
        cv2.imshow("FrameImg", image_overlay)

        cv2.waitKey(0)
        cv2.destroyAllWindows()
    return points, warped_output, clean_output, output_gcode_mask, x_scale, image_overlay


def get_square2(points):
    points = np.array(points)
    x_sort = points[points[:, 0].argsort()]
    left = x_sort[0:2]
    right = x_sort[2:4]
    y_sort_left = left[left[:, 1].argsort()]
    y_sort_right = right[right[:, 1].argsort()]
    pt1 = y_sort_left[0]
    pt2 = y_sort_right[0]
    pt3 = y_sort_right[1]
    pt4 = y_sort_left[1]
    square = np.array([pt1, pt2, pt3, pt4])
    return square

def overlay(img, gcode, show=True, print_result=False):
    if show:
        cv2.namedWindow("results", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO) # dragable windows
    if(len(gcode.shape)>2):
        gcode = cv2.cvtColor(gcode, cv2.COLOR_BGR2GRAY)
    bitwiseOr = cv2.bitwise_or(img, gcode)
    bitwiseXor = cv2.bitwise_xor(img, gcode)
    missing = cv2.bitwise_xor(bitwiseOr, img)
    over = cv2.bitwise_xor(bitwiseXor, missing)
    missing_color = cv2.cvtColor(missing, cv2.COLOR_GRAY2BGR)
    missing_color[missing == 255] = (0, 0, 255) #Highlight underextrusion as red
    over_color = cv2.cvtColor(over, cv2.COLOR_GRAY2BGR)
    over_color[over == 255] = (0,255,255) #Highlight overextrusion as yellow
    print_file_color = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    temp_combined = cv2.addWeighted(missing_color, 1, over_color, 1, 0)
    combined = cv2.addWeighted(temp_combined, 0.5, print_file_color, 0.5, 0) #final visualization

    # adding the boarder effect
    combined_shape = combined.shape[0], combined.shape[1], 3
    border_mask = np.zeros(shape=combined_shape, dtype=np.uint8)
    border_mask2 = np.zeros(shape=combined_shape, dtype=np.uint8)
    ret, threshed = cv2.threshold(gcode, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    contours, hierarchy = cv2.findContours(image=threshed, mode=cv2.RETR_EXTERNAL, method=cv2.CHAIN_APPROX_NONE)
    if contours:
        c = max(contours, key=cv2.contourArea)

    cv2.drawContours(border_mask, [c], 0, (255,255, 255), 60) # look for 40 pixel expansion
    cv2.fillPoly(border_mask2, pts =[c], color=(255,255,255))
    temp_allfilled = cv2.bitwise_or(border_mask, border_mask2)
    border_outer = cv2.bitwise_xor(temp_allfilled, border_mask2)
    border_outer[np.where((border_outer == [255, 255, 255]).all(axis=2))] = [255, 0, 0]
    final_combined = cv2.addWeighted(border_outer, 0.5, combined, 0.5, 0)

    # The filters
    filt_yellow_L = np.array([26, 102, 0])
    filt_yellow_H = np.array([38, 136, 255])

    filt_red_L = np.array([0, 118, 50])
    filt_red_H = np.array([33, 255, 78])

    filt_blue_L = np.array([0, 0, 135])
    filt_blue_H = np.array([255, 255, 222])

    filt_gray_L = np.array([0, 0, 50])
    filt_gray_H = np.array([254, 59, 78])

    hsv = cv2.cvtColor(final_combined, cv2.COLOR_BGR2HSV)

    mask_yellow = cv2.inRange(hsv, filt_yellow_L, filt_yellow_H)
    mask_red = cv2.inRange(hsv, filt_red_L, filt_red_H)
    mask_blue = cv2.inRange(hsv, filt_blue_L, filt_blue_H)
    mask_gray = cv2.inRange(hsv, filt_gray_L, filt_gray_H)

    red = cv2.countNonZero(mask_red)
    yellow = cv2.countNonZero(mask_yellow)
    blue = cv2.countNonZero(mask_blue)
    gray = cv2.countNonZero(mask_gray)

    #list of parameters

    underextruded = red / (red + gray + 1)
    inside_overextruded = yellow / (red + gray + 1)
    outside_overextruded = blue / (red + gray + 1)
    matched = gray / (red + gray + 1)
    # Shift detection autocorrelation
    cov1 = signal.fftconvolve(gcode, gcode[::-1, ::-1], mode='same')
    cov2 = signal.fftconvolve(gcode, img[::-1, ::-1], mode='same')
    center_gcode = np.unravel_index(np.argmax(cov1), cov1.shape)
    center_print = np.unravel_index(np.argmax(cov2), cov2.shape)
    #center_gcode = [ np.average(indices) for indices in np.where(gcode >= 255) ]
    #center_print = [ np.average(indices) for indices in np.where(img >= 255) ]

    x_shift = center_print[1]-center_gcode[1]
    y_shift = center_print[0]-center_gcode[0]
    shift = (int(x_shift), int(y_shift))
    if print_result:
        print(shift, underextruded, inside_overextruded, outside_overextruded, matched)
    total_overextruded = outside_overextruded+inside_overextruded
    to_save = [float(x_shift), float(y_shift), np.round(underextruded, 3), np.round(total_overextruded, 3)]
    if show:
        cv2.imshow("results", final_combined)
        cv2.waitKey(0)
    return shift, final_combined, to_save


def fit_layer_width(template_img, raw_img, gcode, initial_width):
    initial_width = float(initial_width)

    coord, coordDf, coordBound = Gcode_handling.segmentgcode(gcode)
    x_crop, y_crop = match_img(template_img, raw_img)

    gcode_img = np.zeros(shape=raw_img.shape, dtype=np.uint8)

    # draw G-code image
    pixpermm = 0.1
    l_width = int(initial_width/pixpermm)

    for segment in coord:  # plot lines per segment
        x = [item[0] for item in segment]
        y = [item[1] for item in segment]
        y_invert = [0 - n for n in y]
        for i in range(0, len(x) - 1):
            x1, y1 = int((x[i])/pixpermm), int(y_invert[i]/pixpermm+ gcode_img.shape[0])
            x2, y2 = int((x[i+1])/pixpermm), int(y_invert[i+1]/pixpermm+ gcode_img.shape[0])
            cv2.line(gcode_img, (x1, y1), (x2, y2), (255, 255, 255), l_width)

    h, w = template_img.shape[0], template_img.shape[1]
    gcode_img_cropped = gcode_img[y_crop: y_crop+h, x_crop: x_crop+w]


    # Shift detection autocorrelation

    gcode_img_gy = cv2.cvtColor(gcode_img_cropped, cv2.COLOR_BGR2GRAY)
    ret, gcode_img_bi = cv2.threshold(gcode_img_gy, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    template_gy = cv2.cvtColor(template_img, cv2.COLOR_BGR2GRAY)
    ret, template_bi = cv2.threshold(template_gy, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)


    center_gcode = [ np.average(indices) for indices in np.where(gcode_img_bi >= 255) ]
    center_print = [ np.average(indices) for indices in np.where(template_bi >= 255) ]
    x_shift = center_print[1]-center_gcode[1]
    y_shift = center_print[0]-center_gcode[0]

    gcode_img = np.zeros(shape=raw_img.shape, dtype=np.uint8)

    factors = np.arange(0.6, 2, 0.1)
    width_scan = []
    for factor in factors:
        for segment in coord:  # plot lines per segment
            x = [item[0] for item in segment]
            y = [item[1] for item in segment]
            y_invert = [0 - n for n in y]
            seg_width = int(l_width * factor)
            for i in range(0, len(x) - 1):
                x1, y1 = int((x[i])/pixpermm+x_shift), int(y_invert[i]/pixpermm+ raw_img.shape[0]+y_shift)
                x2, y2 = int((x[i+1])/pixpermm+x_shift), int(y_invert[i+1]/pixpermm+ raw_img.shape[0]+y_shift)
                cv2.line(gcode_img, (x1, y1), (x2, y2), (255, 255, 255), seg_width)

        h, w = template_img.shape[0], template_img.shape[1]
        gcode_img_cropped_shifted = gcode_img[y_crop: y_crop+h, x_crop: x_crop+w]

        _, overlay_img, saves = overlay(template_bi, gcode_img_cropped_shifted, show=False, print_result=False)
        diff = abs(saves[2]-saves[3])
        saves.insert(0, factor*l_width*pixpermm)
        saves.append(diff)
        width_scan.append(saves)
        gcode_img = np.zeros(shape=raw_img.shape, dtype=np.uint8)
    min_diff = width_scan[0]
    for i in range(0, len(width_scan)):
        if width_scan[i][5] <= min_diff[5]:
            min_diff = width_scan[i]
    print(min_diff)

    over_extrude_plot = [i[4] for i in width_scan]
    under_extrude_plot = [i[3] for i in width_scan]
    width_in_mm = [i*l_width*pixpermm for i in factors]
    plt.plot(width_in_mm, over_extrude_plot, width_in_mm, under_extrude_plot)
    plt.show()




def match_img(template_color, raw_color):
    # This function produces the new x and y coordinates for the clicks on the wrapped image
    # template_color = cropped image of the printed object
    # raw_color = wrapped image
    # survey_map = survey clicks data from the R pipeline
    template = cv2.cvtColor(template_color, cv2.COLOR_BGR2GRAY)

    raw = cv2.cvtColor(raw_color, cv2.COLOR_BGR2GRAY)
    w, h, = template.shape[::-1]
    res = cv2.matchTemplate(raw, template, cv2.TM_CCOEFF)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = max_loc
    x_shift = top_left[0]
    y_shift = top_left[1]
    return x_shift, y_shift
