import cv2
import Preprocessing
import Video_analysis
from Grid_analysis import *
from tkinter import *
from tkinter import ttk
from tkinter import filedialog
import json
from time import gmtime, strftime
import numpy as np

window = Tk()
window.geometry("700x300")
window.title("Layer 2D")
tab_control = ttk.Notebook(window)

tab1 = ttk.Frame(tab_control)
tab_control.add(tab1, text="2D Auto Analysis")


tab_control.pack(expand=1, fill="both")

# Global variables
# file_name is a Tkinter variable for the image's file path
# file_text is a Tkinter variable for the image text displayed on the UI
# gcode_file is a Tkinter variable for the gcode's file path
# gcode_text is a Tkinter variable for the gcode text displayed on the UI
# gcode_type is a Tkinter variable for the type of gcode file. This is used to fetch specific dimension from a json file

# frame_orig is the original, uncropped, unaltered image frame. This should never be changed
# frame is a copy of Frame_orig which saves any cropping done to the image. However it is not blurred and does not contain a gcode mask
# image_blurred is a copy of Frame which is Gaussian blurred and contains all cropping done previously
# image_binary is the image which is the black and white outline of the image_blurred
# image_warped is the warped frame which is cropped to the four dots
# image_overlay is image with overlay
# image_assessed is image with shifted gcode overlay
# gcode_specs is a dictionary that contains the specific dimensions for the gcode type, like width, x_offset etc.
# gcode_mask is the binary mask created by the gcode. It should be cropped and warped in the same way as frame or image_warped
# crop_frame is a cropped raw frame. It is automatically saved for survey in /Cropped_images
# Non-Global variables
# img is used as a placeholder for when frame is passed to a function
# img_orig is used when frame_orig is passed to a function. But it never alters frame_orig, only frame is altered
# img_bin is a placeholder for image_binary when it is passed to a function
# img_warped is the placeholder for when image_warped is passed to a function
# g_specs is the placeholder for gcode_specs


def browse_files(video=False):
    global frame, image_blurred, frame_orig
    file = filedialog.askopenfilename(initialdir=folder_img,
                                      title="Select a File",
                                      filetypes=(("Images", ('.png', '.jpg', '.mp4')),
                                                 ("all files", ".*")))
    if not file:
        return
    else:
        file_name.set(file)
        file_text.set(file.split('/')[-1])
        file_type = file.split('.')[-1]
        if video:
            return
        else:
            frame_orig = cv2.imread(file_name.get(), 1)
            frame = frame_orig.copy()
            image_blurred = cv2.GaussianBlur(frame, (7, 7), 1)
    update_UI()


def calibrating(img, k):
    factor = Preprocessing.calibration(img, k)
    cal_factor.set(str(round(factor, 5)))


def cropping(img_orig):
    global image_blurred, frame
    frame, topleft, bottomright = Cropping.crop_selection(img_orig)
    image_blurred = cv2.GaussianBlur(frame, (7, 7), 1)


def preprocessing(img_blurred, show=True):
    global image_binary
    img_bgr = cv2.cvtColor(img_blurred, cv2.COLOR_BGR2GRAY)
    threshed, inverted = Preprocessing.Thresh_segment(img_bgr, invert.get(), show)
    invert.set(inverted)
    image_binary = threshed
    update_UI()


def hsv(img_blurred):
    global image_blurred
    threshed, result = Preprocessing.HSV_segment(img_blurred)
    if result:
        image_blurred = threshed




def is_float(e, var, allow_negative=False):
    n = var.get() + str(e.char)
    if allow_negative and n == "-":
        pass
    else:
        try:
            n = float(n)
        except ValueError:
            if e.char != "\x08" and e.char != "":
                return "break"




def browse_gcode():
    global gcode_specs
    file = filedialog.askopenfilename(initialdir = folder_img,
                                      title="Select a File",
                                      filetypes=(("Gcodes", ('.gco', '.gcode', 'csv')),
                                                 ("all files", ".*")))
    if not file:
        return
    else:
        gcode_file.set(file)
        gcode_text.set(file.split('/')[-1])
        gcode_type.set(file.split('.')[-1])

        f = open("Settings.json")
        data_setup = json.load(f)["set-up"]
        if gcode_type.get() == 'csv':
            gcode_specs = data_setup["csv"]
        elif gcode_type.get() == 'gco' or gcode_type.get() == 'gcode':
            gcode_specs = data_setup["gcode"]
        else:
            gcode_specs = []
        f.close()
    update_UI()

def browse_image():
    file = filedialog.askopenfilename(title="Select a File",
                                      filetypes=(("Images", ('.png', '.jpg')),
                                                 ("all files", ".*")))
    if not file:
        return
    else:
        img_file.set(file)
        img_text.set(file.split('/')[-1])


def dot_cal(img_file, gcode_file, g_specs, g_crop, nozzle_size):
    global frame_orig, frame, image_warped, gcode_mask, image_blurred, image_overlay, image_binary, clean_output
    wrap_factor.set(10)
    points, image_warped, clean_output, gcode_mask, scale, image_overlay = Video_analysis.img_cam(img_file, g_specs,
                                                                    gcode_type.get(), gcode_file, g_crop,
                                                                    filament_width=nozzle_size, wrap_factor=wrap_factor.get())


    if len(clean_output) >= 1:
        cal_factor.set(str(round(scale, 5)))
        frame = clean_output
        image_blurred = cv2.GaussianBlur(frame, (7, 7), 1)
        preprocessing(image_blurred, show=False)
        evaluate_print(image_binary, gcode_mask, show=False)
    update_UI()

def crop_evaluate():
    global image_assessed, crop_frame, crop_gcode_mask
    frame, top_left, bottom_right = Cropping.crop_selection(image_overlay)
    crop_image_binary = image_binary[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
    crop_gcode_mask = gcode_mask[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
    crop_frame = clean_output[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]

    evaluate_print(crop_image_binary, crop_gcode_mask, show=False)
    num_rows, num_cols = crop_gcode_mask.shape[:2]

    translation_matrix = np.float32([[1, 0, -shift[0]], [0, 1, -shift[1]]])
    img_translation = cv2.warpAffine(crop_gcode_mask, translation_matrix, (num_cols, num_rows))
    shifted, image_assessed, to_save = Video_analysis.overlay(crop_image_binary, img_translation, show=True)
    print('Over-extrusion: ' + str(to_save[3]))
    print('Under-extrusion: ' + str(to_save[2]))
    update_UI()


def evaluate_print(img_bin, gcode_masked, show=True):
    global shift
    shift, _, to_save = Video_analysis.overlay(img_bin, gcode_masked, show=show)
    if len(shift) > 0:
        update_UI()

def correct_shift_gcode2():
    global image_assessed
    num_rows, num_cols = gcode_mask.shape[:2]
    translation_matrix = np.float32([[1, 0, -shift[0]], [0, 1, -shift[1]]])
    img_translation = cv2.warpAffine(gcode_mask, translation_matrix, (num_cols, num_rows))
    shifted, image_assessed, to_save = Video_analysis.overlay(image_binary, img_translation, show=True)
    to_save = [strftime("%Y-%m-%d %H:%M:%S", gmtime()),
               file_text.get(), to_save[0], to_save[1], to_save[2], to_save[3]]

    update_UI()


def getinfill_img(img):
    infill = find_img_infill(img)
    print(str(file_text.get()) + " Infill:" + str(infill))

def getinfill_gcode(gcode):
    infill = find_gcode_infill(gcode)
    print(str(gcode_text.get()) + " Infill:" + str(infill))



def grid_analysis(final_assessment, grid_size):
    global grid_results
    grid_size = int(grid_size)
    #grid = img_to_grid(final_assessment, row, col)
    grid = img_to_grid2(final_assessment, grid_size)
    grid_results = cal_grid(grid, image_assessed)
    heatmap = grid_to_heatmap(data = grid_results, img = crop_frame )


def visual_under(crop_img, final_assessment):
    visualize_under(crop_img, final_assessment, show=True)


def update_UI():
    global frame, image_binary, gcode_mask, width_data
    # If both gcode and image are selected, overlay gcode button will be active
    if len(gcode_file.get()) > 0 and len(frame) > 1:
        bt_dotcal.config(state='normal')
    else:
        bt_dotcal.config(state='disabled')

    # If there is an image selected, buttons for calculating scale and cropping and segmentation are enabled
    if len(frame) > 1:

        bt_infill_img.config(state="normal")
    else:

        bt_infill_img.config(state="disabled")

    if len(gcode_mask) > 1:
        bt_infill_gcode.config(state="normal")
    else:
        bt_infill_gcode.config(state="disabled")
    # If the thresholding has been done AND there is a gcode mask, enable buttons for gcode overlay
    if len(image_binary) > 1 and len(gcode_mask) > 1:
        bt_compare.config(state="normal")
    else:
        bt_compare.config(state="disabled")

    if len(image_assessed) > 1:
        bt_under.config(state='normal')
        bt_grid.config(state='normal')
    else:
        bt_under.config(state='disabled')
        bt_grid.config(state='disabled')


folder_result = StringVar(tab1)
folder_result.set("Demo files")
folder_img = "Demo files"
file_name = StringVar(tab1)
file_text = StringVar(tab1)
file_text.set("No file selected")
scale_vis = StringVar(tab1)
scale_vis.set('3')
gcode_crop = StringVar(tab1)
gcode_crop.set('5')
cal_factor = StringVar(tab1)
cal_factor.set('0.1')
img_file = StringVar(tab1)
img_text = StringVar(tab1)
img_text.set("No image selected")
result_folder = StringVar(tab1)
result_folder.set('Results/')
gcode_file = StringVar(tab1)
gcode_text = StringVar(tab1)
gcode_text.set("No gcode selected")
gcode_type = StringVar(tab1)
nozzle_size = StringVar(tab1)
nozzle_size.set('1.5')
grid_size = StringVar(tab1)
grid_size.set('7')
wrap_factor = IntVar(tab1)
wrap_factor.set(round(1/float(cal_factor.get())))


shift = []
invert = IntVar(tab1)
invert.set(0)
image_warped = []
image_overlay = []
image_assessed = []
gcode_mask = []
gcode_specs = []
frame_orig = []
frame = []
crop_frame = []
output_csv = StringVar(tab1)
image_blurred = []
image_binary = []


#####################TAB 1############################################################
## WORKING TAB ###

# Row: Select file and image scale
r_file_and_gcode = 1
dr_file = Button(tab1, text="Select Image", command=browse_files)
dr_file.grid(row=r_file_and_gcode, column=1, pady=5, padx=20, sticky=W)

lb_file = Label(tab1, textvariable=file_text)
lb_file.grid(row=r_file_and_gcode, column=2, columnspan=2, pady=5, padx=20, sticky=W)

dr_gcode = Button(tab1, text="Select Gcode", command=browse_gcode)
dr_gcode.grid(row=r_file_and_gcode, column=3, pady=5, padx=20, sticky=W)

lb_gcode = Label(tab1, textvariable=gcode_text)
lb_gcode.grid(row=r_file_and_gcode, column=4, columnspan=2, pady=5, padx=20, sticky=W)

# Row: Gcode variables
r_gcode = 2

lb_g_crop = Label(tab1, text="Gcode crop by (mm):")
lb_g_crop.grid(row=r_gcode, column=1, pady=5, padx=20, sticky=W)

en_g_crop = Entry(tab1, textvariable=gcode_crop)
en_g_crop.bind('<KeyPress>', lambda e: is_float(e, gcode_crop, allow_negative=True))
en_g_crop.grid(row=r_gcode, column=2, pady=5, padx=10, sticky=W)


bt_dotcal = Button(tab1, text="Overlay Gcode", command=lambda: dot_cal(file_name.get(), gcode_file.get(), gcode_specs,
                                                                       gcode_crop.get(), nozzle_size=nozzle_size.get()), state="disabled")
bt_dotcal.grid(row=r_gcode, column=3, pady=5, padx=20, sticky=W)


# Row: compare with Gcode
r_compare = 4
bt_compare = Button(tab1, text='Crop to analyze', command=lambda: crop_evaluate(), state="disabled")
bt_compare.grid(row=r_compare, column=1, pady=5, padx=20, sticky=W)

bt_under = Button(tab1, text='Visualize Under-extrusion', command = lambda:visualize_under(crop_frame, image_assessed),
                  state="disabled")
bt_under.grid(row=r_compare, column=2, pady=5, padx=20, sticky=W)



# Row: Grid analysis
r_grid = 5
r_grid2 = 6
lb_grid_size = Label(tab1, text="Grid Size")
lb_grid_size.grid(row=r_grid, column=1, pady=5, padx=20, sticky=W)

en_grid_size = Entry(tab1, textvariable=grid_size)
en_grid_size.bind('<KeyPress>', lambda e: is_float(e, grid_size))
en_grid_size.grid(row=r_grid, column=2, pady=5, padx=2, sticky=E)


bt_grid = Button(tab1, text='Grid Analysis', command = lambda: grid_analysis(image_assessed, grid_size=grid_size.get()),
                 state="disabled")
bt_grid.grid(row = r_grid2, column=1, pady=5, padx=20, sticky=W)


# Row: infill
r_infill = 7

bt_infill_img = Button(tab1, text='In fill image', command = lambda: getinfill_img(image_blurred), state="disabled")
bt_infill_img.grid(row = r_infill, column=1, pady=5, padx=20, sticky=W)

bt_infill_gcode = Button(tab1, text='In fill gcode', command = lambda: getinfill_gcode(gcode_mask), state="disabled")
bt_infill_gcode.grid(row = r_infill, column=2, pady=5, padx=20, sticky=W)

# Row: credit
r_credit = 8
lb_credits = Label(tab1, text = "Authors: Jelle Potappel & Yizhou Ma (Wageningen University)")
lb_credits.grid(row=r_credit, column=3, columnspan=2, pady=5, padx=20, sticky=W)




window.mainloop()
