# Layer 2D: layer-wise assessment of 3D printing accuracy

This is a software developed by the Food Process Engineering Chair Group at Wageningen University, the Netherlands. The Layer 2D software performs image analysis to measure layer-wise 3D printing accuracy. It is the complementary software to a [journal publication](https://doi.org/10.1016/j.crfs.2023.100511).   

## User Guide

The current version of software can only be run with the required python modules installed (see requirements.txt for details). We recommend running the scripts in PyCharm or other IDE. 

### Image requirements

Sample layer images can be found in the `Demo files` folder. The images require to include 4 visual markers which define the boundary area. The boundary area can be set in the `settings.json` file in mm. If the boundary area is different from the printer plate size defined in the G-code slicing software, the x_offset and y_offset should be specified. See this [reference example](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&title=Untitled%20Diagram.drawio#R5ZhNc5swEIZ%2FjY%2BeQchgfIyd9GM6PXkmbY%2BKJUCtQETINe6v72KEDQbX6UxFqHOKeFes4Nk3a4kJXiXFe0Wy%2BLOkTExchxYTfD9xXeRiD%2F6Uyr5S%2FJlfCZHi1Ew6CWv%2BixnRMeqWU5a3JmopheZZW9zINGUb3dKIUnLXnhZK0V41IxHrCOsNEV31C6c6rtTAc076B8ajuF4ZOSaSkHqyEfKYULlrSPhhgldKSl2NkmLFRAmv5lLd9%2B5C9PhgiqX6JTfgvSJT9Bx9Uh%2B%2F5zRBj9Ejm6J5leYnEVvzxuZp9b5GECm5zcw0pjQr%2BsCTp3q6030wdHxd8AmTCdNqD1PqRDNzi7FIfbk78fYWRosbrHFdBGJqHB1TnzDAwJD4GyrOdSoAJaWszOJM8HIXc83WGdmU0R38K4AW6wRWvUcwvEivSekPBeqye0U4%2F5gNJXl8mGsHlEkzv%2BqxGe7B6Pq2MLrXMUIWaHPsOkKSZ1XvC3lRolyGXIiVFFIdEmGEQ9eZg55rJX%2BwRiRwn7Dv2wSPgjZ53CXv9oG3xR3b5G6N4izwRkXRG9K9NAyQOw739jWOQcH7%2F6V9%2FXH1gBdsesYP0V28MsXgjfyCja33Lt5I750tRtZ7Ud%2FW1xew7PIJBpE%2B0KiEUMKrN2viP29lHZjmh3PuHUxAflZUt5l4nag6bZps8LhVwvYiIDcWPvMAlEO3i92uYSpTdlZwIxHBoxQuN1A%2BBvqyLC6HM%2FGdCSScUnHJXe3N%2FzC7e4zaVkFdp%2Fg9TsHWnNK3u7flFPP9oc6velxyC%2BZRUhPNZZli4QzZ71FPwx%2FWTX1nFltu%2BgpBGYY50zfmH2t%2BOX7yM37xunaZD2qXvsOZLbt8u1W7DNduzrc5gTX%2FwOXpe%2FMh1vhqjx9%2BAw%3D%3D) for details.

You can place the visual markers to any rectangular orientation on the printing plate as long as their positions are specified in the `settings.json` file. Please make sure that your printed object is within the visual marker area. 

When taking the layer image, we recommend you to place the camera right above the printing plate. Make sure that the 4 visual markers and the printed layers are properly included in the frame.  


### Gcode requirements

We developed the gcode parser based on the [Slic3r](https://slic3r.org/)-styled gcodes. **The software currently is not cmmpatible with other styles of gcodes.** The parsing algorithm can be easily updated in the `Gcode\gcode_handling.py`. The function `segmentgcode` sets all the parsing rules based on the specific gcode style. In future updates, we may include other popular gcode styles in the parsing function. 

### Navigating the UI 

Once the python environment is set up, you can run  `main.py` to start the software UI. 

- `Select Image` launches a file dialog to select the image that fulfills the image requirement listed above.

- `Select Gcode` launches a file dialog to select the matching Gcode file that created the structure in the image.

- `Gcode crop by (mm)` specifies the cropping edge around the structure to automatically crop the area of interest. 

- `Overlay Gcode` performs a quick check of the gcode-generated layer overlayed on top of the object image. Press **Q** to close the pop-up window. 

- `Crop to analyze` is designed to select the structure of interest on the cropped image. Drag and drop to select the area of interest and press **Q** to exist the cropping function. An aligned overlayed image will automatically appear and the Python console will return the over and under-extrusion fractions relative to the target area. You can close the window by press the cross icon on the window. 

- `Visualize Under-extrusion` is to visualize the under-extruded area highlighted in yellow. Press the cross icon to close the window. 

- `Grid Analysis` creates an over-extrusion heatmap which highlights the severity of the over-extrusion. The higher the intensity, the greater fraction of over-extrusion in the given grid (which can be specified by the `Grid Size` input)

- `Infill image` finds the solid fraction of a closed structure on an object image. A cropping step is required to select the area of interest. Press **Q** to exist cropping and the infill % is shown in the Python console. 

- `Infill gocde` finds the solid fraction of a closed structure on a gcode-generated image. Click and drag to select the area of interest, and press **Q** to exist cropping and find out the infill %. 

#### A demo video can be found [here](https://www.youtube.com/watch?v=fxTexnPfWXA&ab_channel=YBenMa). 

## Contacts

If you have questions about the software or would like to contribute to this open-source software development, please contact us at yizhou.ma@wur.nl

## Citation

Please cite this work as:
Ma, Y., Potappel, J., Schutyser, M. A., Boom, R. M., & Zhang, L. (2023). Quantitative analysis of 3D food printing layer extrusion accuracy: Contextualizing automated image analysis with human evaluations: Quantifying 3D food printing accuracy. Current Research in Food Science, 6, 100511.
