import cv2
import numpy as np

cap = cv2.VideoCapture(0)

def empty(a):
    pass

cv2.namedWindow("Parameter")
cv2.resizeWindow("Parameter", 640, 200)
cv2.createTrackbar("Threshold", "Parameter", 0, 255, empty)

img_counter = 0
while True:
    _, frame = cap.read()
    imgBlur = cv2.GaussianBlur(frame, (7,7), 1)
    imgGray = cv2.cvtColor(imgBlur, cv2.COLOR_BGR2GRAY)
    thresh = cv2.getTrackbarPos("Threshold", "Parameter")
    maxValue = 255
    th, dst = cv2.threshold(imgGray, thresh, maxValue, cv2.THRESH_BINARY_INV)

    cv2.imshow("Frame", dst)

    key = cv2.waitKey(1)
    if key == 32:
        img_name = "snap_{}.png".format(img_counter)
        cv2.imwrite(img_name, frame)
        img_counter += 1

    if key == 27:
        outFile = open("threshold.txt", "w")
        outFile.write(str(thresh))
        outFile.close()
        break

cap.release()

cv2.destroyAllWindows()
