import cv2, math
import numpy as np
from Video_analysis import overlay
import pandas as pd
from Gcode.Gcode_handling import segmentgcode
from Grid_analysis import img_to_grid2, cal_grid, grid_to_heatmap,survey_to_heatmap2

def find_gcode_layer(template_color, raw_color, coord, linewidth, pixpermm, show=False):
    # This function produces the new x and y coordinates for the clicks on the wrapped image
    # template_color = cropped image of the printed object
    # raw_color = wrapped image
    # survey_map = survey clicks data from the R pipeline
    template = cv2.cvtColor(template_color, cv2.COLOR_BGR2GRAY)
    template = cv2.flip(template, 0)
    template = cv2.rotate(template, cv2.ROTATE_90_COUNTERCLOCKWISE)
    w, h = template.shape[::-1]

    raw = cv2.cvtColor(raw_color, cv2.COLOR_BGR2GRAY)
    res = cv2.matchTemplate(raw, template, cv2.TM_CCOEFF)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)
    top_left = max_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)

    gcode_img = np.zeros(shape=raw.shape, dtype=np.uint8)
    gcode_img = cv2.flip(gcode_img, 0)
    width = int(linewidth / pixpermm)
    for segment in coord:  # plot lines per segment
        x = [item[0] for item in segment]
        y = [item[1] for item in segment]
        y_invert = [0 - n for n in y]
        for i in range(0, len(x) - 1):
            x1, y1 = int((x[i]) / pixpermm), int(y[i] / pixpermm)
            x2, y2 = int((x[i + 1]) / pixpermm), int(y[i + 1] / pixpermm)
            cv2.line(gcode_img, (x1, y1), (x2, y2), (140, 217, 245), width)
    cropped_gcode_img=gcode_img[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
    if show:
        cv2.namedWindow("matched_gcode", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)  # dragable windows
        cv2.imshow('matched_gcode', cropped_gcode_img)

        cv2.waitKey(0)
        cv2.destroyAllWindows()
    return cropped_gcode_img

def run_heatmap(template_color, cropped_gcode_img, grid_size, quantile):

    template_color = cv2.flip(template_color, 0)
    template_color = cv2.rotate(template_color, cv2.ROTATE_90_COUNTERCLOCKWISE)
    template = cv2.cvtColor(template_color, cv2.COLOR_BGR2GRAY)
    ret, threshed = cv2.threshold(template, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    shift, final_combined, to_save = overlay(threshed, cropped_gcode_img, show=False)
    grids = img_to_grid2(final_combined, grid_size)
    results = cal_grid(grids, threshed)
    _, heatmap_cv = grid_to_heatmap(results, template_color, quantile=quantile,over=True, under=False, show=True)
    return heatmap_cv

def correlation_coefficient(T1, T2):
    numerator = np.mean((T1 - T1.mean()) * (T2 - T2.mean()))
    denominator = T1.std() * T2.std()
    if denominator == 0:
        return 0
    else:
        result = numerator / denominator
        return result

heatmap = pd.read_csv('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Survey/Cleaned heatmap survey/HC20_fresh_heatmap.csv')
img = cv2.imread('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Cropped_images/For survey/shorten_HC20_fresh_cropped.jpg')
raw_img = cv2.imread('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Cropped_images/shorten_HC20_fresh_wrapped.jpg')
gcode = 'C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Cookie Gcodes/so_Cube cookie_hc_20 X 2.gcode'
coord, coordDf, coordBound = segmentgcode(gcode)
cropped_gcode_img = find_gcode_layer(img, raw_img, coord, 1.5, 0.1)

_, heatmap_survey = survey_to_heatmap2(img, heatmap, show=True)
heatmap_survey = np.matrix.transpose(heatmap_survey)
heatmap_cv = run_heatmap(img, cropped_gcode_img, 14, 99.5)

#size_list = list(range(5,15))
#q_list = np.arange(95, 100, 0.5).tolist()
#
#all_results = []
#for n in size_list:
#    for q in q_list:
#        heatmap_cv = run_heatmap(img, cropped_gcode_img, n, q)
#        heatmap_cv = heatmap_cv[:, :, 0]
#        result = correlation_coefficient(heatmap_cv, heatmap_survey)
#        all_results.append([n, q, result])
#df = pd.DataFrame(all_results)
#df.columns = ['grid_size', 'Quantile', 'Score']
#df2 = df.sort_values(by='Score', ascending=False)
#print(df2)

