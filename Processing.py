import cv2
import numpy as np
from skimage.morphology import medial_axis
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from scipy.stats import ks_2samp


def width_assesment(frame, gcode, f, sc):
    while True:
        # Compute the medial axis (skeleton) and the distance transform
        normed = cv2.normalize(frame, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
        medial_img, m_distance = medial_axis(normed, return_distance=True)
        m_distance_px = m_distance
        m_distance = m_distance * float(f)

        medial_distance_px = m_distance_px * medial_img
        medial_distance = m_distance * medial_img

        medial_widths = np.around(medial_distance * 2)

        medial_widths_float_px = medial_distance_px * 2
        medial_widths_float = medial_distance * 2

        # Plot the image and results
        h_bins = int(np.amax(medial_widths[medial_widths != 0]))+1
        h_start = 0
        h_step = 0.5

        cmax = int(np.amax(medial_widths[medial_widths != 0]))

        width_lines = medial_widths_float[medial_widths_float > 0.5]

        coords = np.column_stack(np.where(medial_widths_float > 0))

        lengths_px = medial_widths_float_px[np.nonzero(medial_widths_float_px)]
        lengths = medial_widths_float[np.nonzero(medial_widths_float)]

        data = np.hstack((np.flip(coords, 1), np.reshape(lengths, (-1, 1)), np.reshape(lengths_px, (-1, 1))))
        data = data.astype(float)
        data = np.around(data, 4)
        widths = [i * float(sc) for i in data[:, 2]]
        if len(gcode) > 1:
            gcode = gcode[:, :, 0]
            # Compute the medial axis (skeleton) and the distance transform
            normed_g = cv2.normalize(gcode, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
            medial_g, m_distance_g = medial_axis(normed_g, return_distance=True)
            m_distance_px_g = m_distance_g
            m_distance_g = m_distance_g * float(f)
            medial_distance_px_g = m_distance_px_g * medial_g
            medial_distance_g = m_distance_g * medial_g
            medial_widths_g = np.around(medial_distance_g * 2)
            medial_widths_float_px_g = medial_distance_px_g * 2
            medial_widths_float_g = medial_distance_g * 2
            # Plot the image and results
            h_bins_g = int(np.amax(medial_widths_g[medial_widths_g != 0])) + 1
            if int(np.amax(medial_widths_g[medial_widths_g != 0])) > int(np.amax(medial_widths[medial_widths != 0])):
                h_bins = h_bins_g
                cmax = int(np.amax(medial_widths_g[medial_widths_g != 0]))
            else:
                pass
            width_lines_g = medial_widths_float_g[medial_widths_float_g != 0]
            coords_g = np.column_stack(np.where(medial_widths_float_g > 0))
            lengths_px_g = medial_widths_float_px_g[np.nonzero(medial_widths_float_px_g)]
            lengths_g = medial_widths_float_g[np.nonzero(medial_widths_float_g)]
            data_g = np.hstack((np.flip(coords_g, 1), np.reshape(lengths_g, (-1, 1)), np.reshape(lengths_px_g, (-1, 1))))
            data_g = data_g.astype(float)
            data_g = np.around(data_g, 4)
            widths_g = [i * float(sc) for i in data_g[:, 2]]
            fig = make_subplots(rows=2, cols=2, start_cell="top-left",
                                subplot_titles=["Print width", "Print width distribution", "Gcode width",
                                                "Gcode width distribution"])
            fig.add_trace(go.Scatter(
                x=data_g[:, 0],
                y=data_g[:, 1],
                mode="markers",
                hovertemplate=
                "(%{x},%{y})<br>" +
                "%{text}" +
                "<extra></extra>",
                text=["Width: {} mm".format(i) for i in data_g[:, 2]],
                marker=dict(
                    size=widths_g,
                    line=dict(width=0),
                    color=data_g[:, 2],
                    opacity=0.4,
                    colorbar=dict(
                        title="Width [mm]",
                        titleside="top",
                        ticks="outside"
                    )
                )),
                row=2, col=1
            )
            fig.update_layout(
                plot_bgcolor="white",
                paper_bgcolor="#F8F8F8",
                yaxis=dict(
                    scaleanchor="x",
                    scaleratio=1,
                    autorange="reversed",
                    showgrid=False,
                    ticks='outside'
                ),
                xaxis=dict(
                    showgrid=False,
                    ticks='outside'
                ),
                showlegend=False
            )
            # fig1.show()
            # fig2 = go.Figure()
            fig.add_trace(go.Histogram(
                x=width_lines_g,
                xbins=dict(start=h_start, size=h_step, end=h_bins),
                hovertemplate=
                "Width: %{x} mm<br>" +
                "Count: %{y}" +
                "<extra></extra>",
            ),
                row=2, col=2
            )
            fig.update_layout(
                plot_bgcolor="white",
                paper_bgcolor="#F8F8F8",
                xaxis=dict(
                    ticks='outside'
                ),
                yaxis=dict(
                    ticks='outside'
                )
            )
        else:
            fig = make_subplots(rows=1, cols=2, start_cell="top-left",  subplot_titles=["Print width", "Print width distribution"])

        fig.add_trace(go.Scatter(
            x=data[:, 0],
            y=data[:, 1],
            mode="markers",
            hovertemplate=
            "(%{x},%{y})<br>" +
            "%{text}" +
            "<extra></extra>",
            text=["Width: {} mm".format(i) for i in data[:, 2]],
            marker=dict(
                size=widths,
                line=dict(width=0),
                color=data[:, 2],
                opacity=0.4,
                cmin=0,
                cmax=cmax,
                colorbar=dict(
                    title="Width [mm]",
                    titleside="top",
                    ticks="outside"
                )
            )),
            row=1, col=1
        )
        fig.update_layout(
            plot_bgcolor="white",
            paper_bgcolor="#F8F8F8",
            yaxis = dict(
                scaleanchor="x",
                scaleratio=1,
                autorange="reversed",
                showgrid=False,
                ticks='outside'
            ),
            xaxis = dict(
                showgrid=False,
                ticks='outside'
            ),
            showlegend=False
        )
        fig.add_trace(go.Histogram(
            x=width_lines,
            xbins=dict(start=h_start, size=h_step, end=h_bins),
            hovertemplate =
            "Width: %{x} mm<br>" +
            "Count: %{y}" +
            "<extra></extra>",
            ),
            row=1, col=2
        )
        fig.update_layout(
            plot_bgcolor="white",
            paper_bgcolor="#F8F8F8",
            xaxis = dict(
                ticks='outside'
            ),
            yaxis = dict(
                ticks='outside'
            ),
            showlegend=False,
            coloraxis_colorbar_x=-1
        )
        fig.show()

        cv2.destroyAllWindows()
        print_mean=np.mean(width_lines)
        print_std=np.std(width_lines)
        print(print_mean, print_std, len(widths))
        return data