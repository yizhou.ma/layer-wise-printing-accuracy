import cv2, math
import numpy as np
from stl import mesh

from Gcode.Gcode_handling import segmentgcode
from skimage import measure
import math

def simulate_stl(gcode, ref_img, pixpermm):
    # parse Gcode and creat an empty reference image
    coord, coordDf, coordBound = segmentgcode(gcode)
    print(coord)
    gcode_img = np.zeros(shape=ref_img.shape, dtype=np.uint8)
    # extract a list of layer heights
    temp_z = []
    for segment in coord: # plot lines per segment
        z = [item[2] for item in segment]
        temp_z.append(np.unique(z))
    layer_z = np.unique(temp_z)
    layer_height = layer_z[2]-layer_z[1]

    # Calculate line width based on layer height and filament width
    #l_width = 3.14 * (filament_width ** 2) / (4 * layer_height) + (1 - 3.14 / 4) * layer_height
    #pix_width = int(l_width / pixpermm)

    # Loop to generate layer-wise G-code images
    z_ref = layer_z[0]
    width_all = []
    gcode_stack_list = []
    for segment in coord:
        x = [item[0] for item in segment]
        y = [item[1] for item in segment]
        z = [item[2] for item in segment]
        e = [item[3] for item in segment]
        y_invert = [0 - n for n in y]
        for i in range(0, len(x) - 1):
            if z[i] == z_ref:
                x1, y1 = x[i], y[i]
                x2, y2 = x[i + 1], y[i + 1]
                dis = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
                w = 3.14 * (27 ** 2) * (e[i + 1] - e[i]) / (4 * layer_height * dis) + (1 - 3.14 / 4) * layer_height
                width_all.append(round(w, 3))
                #L = (e[i + 1] - e[i]) * (27 ** 2 / 1.2 ** 2)
                #w = 3.14 * L * (filament_width ** 2) / (4 * layer_height * dis) + (1 - 3.14 / 4) * layer_height
                l_width = int(round(w / pixpermm))
                if l_width != 0:
                    x1, y1 = int((x[i]) / pixpermm), int(y_invert[i] / pixpermm + ref_img.shape[0])
                    x2, y2 = int((x[i + 1]) / pixpermm), int(y_invert[i + 1] / pixpermm + ref_img.shape[0])
                    cv2.line(gcode_img, (x1, y1), (x2, y2), (140, 217, 245), l_width)

            else:
                x1, y1 = x[i], y[i]
                x2, y2 = x[i + 1], y[i + 1]
                dis = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
                w = 3.14 * (27 ** 2) * (e[i + 1] - e[i]) / (4 * layer_height * dis) + (1 - 3.14 / 4) * layer_height
                width_all.append(round(w, 3))

                #L = (e[i + 1] - e[i]) * (27 ** 2 / 1.2 ** 2)
                #w = 3.14 * L * (filament_width ** 2) / (4 * layer_height * dis) + (1 - 3.14 / 4) * layer_height
                l_width = int(round(w / pixpermm))
                if l_width != 0:
                    x1, y1 = int((x[i]) / pixpermm), int(y_invert[i] / pixpermm + ref_img.shape[0])
                    x2, y2 = int((x[i + 1]) / pixpermm), int(y_invert[i + 1] / pixpermm + ref_img.shape[0])
                    cv2.line(gcode_img, (x1, y1), (x2, y2), (140, 217, 245), l_width)

                gcode_img = cv2.cvtColor(gcode_img, cv2.COLOR_BGR2GRAY)
                gcode_stack_list.append(gcode_img)
                gcode_img = np.zeros(shape=ref_img.shape, dtype=np.uint8)
                z_ref = z[i]
            if z[i] == layer_z[-1]:
                gcode_img = np.zeros(shape=ref_img.shape, dtype=np.uint8)
                x1, y1 = x[i], y[i]
                x2, y2 = x[i + 1], y[i + 1]
                dis = math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
                w = 3.14 * (27 ** 2) * (e[i + 1] - e[i]) / (4 * layer_height * dis) + (1 - 3.14 / 4) * layer_height
                width_all.append(round(w, 3))

                #L = (e[i + 1] - e[i]) * (27 ** 2 / 1.2 ** 2)
                #w = 3.14 * L * (filament_width ** 2) / (4 * layer_height * dis) + (1 - 3.14 / 4) * layer_height
                l_width = int(round(w / pixpermm))
                if l_width != 0:
                    x1, y1 = int((x[i]) / pixpermm), int(y_invert[i] / pixpermm + ref_img.shape[0])
                    x2, y2 = int((x[i + 1]) / pixpermm), int(y_invert[i + 1] / pixpermm + ref_img.shape[0])
                    cv2.line(gcode_img, (x1, y1), (x2, y2), (140, 217, 245), l_width)

                gcode_img = cv2.cvtColor(gcode_img, cv2.COLOR_BGR2GRAY)
    gcode_stack_list.append(gcode_img)
    width_mode = max(set(width_all), key=width_all.count)

    stacked_image = np.zeros(shape=gcode_img.shape, dtype=np.uint8)
    for n, image in enumerate(gcode_stack_list):
        stacked_image = cv2.bitwise_or(stacked_image, image)

    # create 3D-mask
    mask = np.dstack(gcode_stack_list)
    mask[:,:,-1] = 0
    mask[:,:, 0] = 0
    verts, faces, normals, values = measure.marching_cubes(mask)
    newvert = []
    for i, line in enumerate(verts):
        newvert.append([line[0]*pixpermm, line[1]*pixpermm, line[2]*layer_height])

    newvert = np.array(newvert)

    obj_3D = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(faces):
        obj_3D.vectors[i] = newvert[f]

    return obj_3D, stacked_image, width_all, width_mode



gcode = 'C:/Users/ma057/OneDrive - WageningenUR/WUR/Material-driven Gcode/Die-swell measurements/die-swell gcodes/CE3S1_Cube cookie_line infill_10mm_distance.gcode'

ref_img = cv2.imread('C:/Users/ma057/OneDrive - WageningenUR/WUR/Cookie Study/Cropped_images/shorten_HC20_baked_wrapped.jpg')
ref_img = cv2.flip(ref_img, 0)

stl, stacked_image, width_all, width_mode = simulate_stl(gcode, ref_img, pixpermm=0.1)
print(width_mode)
cv2.namedWindow("test", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)  # dragable windows

cv2.imshow('test', stacked_image)
cv2.waitKey(0)
cv2.destroyAllWindows()

#stl.save('C:/Users/ma057/OneDrive - WageningenUR/WUR/Material-driven Gcode/Die-swell measurements/die-swell gcodes/3mm_block_coco.stl')
