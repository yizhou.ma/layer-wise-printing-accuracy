import re
import numpy as np
import pandas as pd
import plotly.express as px
import cv2
import csv

points_gcode = []
points_img = []
points_cam = []
points_distance = []

# This function takes a gcode file and convert it into segmented X and Y coordinates
# segby: default is G92, but also possible to segment by G0
def segmentgcode(file, segby = 'G92'):
    file = open(file,'r')
    g1pattern = r"^G1.*"
    g92pattern = r"^G92.*"
    g0pattern = r"^G0.*"
    g1re = re.compile(g1pattern)
    g92re = re.compile(g92pattern)
    g0re = re.compile(g0pattern)

    Xpattern = r".*X[-\.\d]+"
    Ypattern = r".*Y[-\.\d]+"
    Zpattern = r".*Z[-\.\d]+"
    Epattern = r".*E[-\.\d]+"
    Fpattern = r".*F[-\.\d]+"
    Xre = re.compile(Xpattern)
    Yre = re.compile(Ypattern)
    Zre = re.compile(Zpattern)
    Ere = re.compile(Epattern)
    Fre = re.compile(Fpattern)

    gcode = file.readlines()

    # Find the segments based on G92
    indices = []
    for num,line in enumerate(gcode):
        if g92re.match(line):
            indices.append(num)
        #if g1re.match(line) and Xre.match(line) and Fre.match(line):
        #    if not Ere.match(line):
        #        indices.append(num)

    if segby == 'G0': # for special cases, segmentations are G0
        indices = []
        for num, line in enumerate(gcode):
            if g0re.match(line):
                indices.append(num)

    # Make gcode numpy array
    gcodedf = []
    for num,line in enumerate(gcode):
        gcodedf.append(line)

    gcodedf = np.array(gcodedf)

    # Split the Gcode based on the segments
    gcodesg = []
    prev = 0
    for index in indices:
        gcodesg.append(gcodedf[prev:index])
        prev = index

    gcodesg.append(gcodedf[indices[-1]:])

    Z = []
    F = []
    coord = []
    for segment in gcodesg:
        coordsub = []
        tempX = []
        tempY = []
        tempE = 0
        X = []
        Y = []
        check = False
        for line in segment:
            if g0re.match(line) and Zre.match(line):
                Z = float(re.findall(Zpattern, line)[0].split('Z')[1])
            if g1re.match(line) and Zre.match(line): # check layer height Z
                Z = float(re.findall(Zpattern, line)[0].split('Z')[1])

            if g1re.match(line) and Fre.match(line): # check layer height Z
                F = float(re.findall(Fpattern, line)[0].split('F')[1])

            if g1re.match(line) and Ere.match(line) and Fre.match(line): # check layer height Z
                if tempE == 0:
                    tempE = float(re.findall(Epattern, line)[0].split('E')[1])

            if g0re.match(line) and Xre.match(line) and Yre.match(line): # check if initial move G0 is included
                if not Ere.match(line):
                    tempX = float(re.findall(Xpattern, line)[0].split('X')[1])
                    tempY = float(re.findall(Ypattern, line)[0].split('Y')[1])
                    check = True
                if Ere.match(line):
                    if check:
                        coordsub.append([tempX, tempY, Z, F])
                        check = False
                    X = float(re.findall(Xpattern, line)[0].split('X')[1])
                    Y = float(re.findall(Ypattern, line)[0].split('Y')[1])
                    E = float(re.findall(Epattern, line)[0].split('E')[1])
                    coordsub.append([X, Y, Z, E, F])

            if g1re.match(line) and Xre.match(line) and Yre.match(line): # take X and Y coordinates
                if not Ere.match(line):
                    tempX = float(re.findall(Xpattern, line)[0].split('X')[1])
                    tempY = float(re.findall(Ypattern, line)[0].split('Y')[1])
                    check = True
                if Ere.match(line):
                    if check:
                        if tempE == 0:
                            tempE = 1
                            coordsub.append([tempX, tempY, Z, tempE])
                        else:
                            coordsub.append([tempX, tempY, Z, tempE])
                        check = False

                    X = float(re.findall(Xpattern, line)[0].split('X')[1])
                    Y = float(re.findall(Ypattern, line)[0].split('Y')[1])
                    E = float(re.findall(Epattern, line)[0].split('E')[1])
                    coordsub.append([X, Y, Z, E, F])
                    tempE = E

        coord.append(coordsub)

    coord = [x for x in coord if x != []] #remove empty lists
    coordDf = []
    for n, segment in enumerate(coord): #convert list to pandaDf
        tempDf = pd.DataFrame(segment, columns = ['X', 'Y', 'Z', 'E', 'F'])
        tempDf['Step'] = n
        coordDf.append(tempDf)
    coordDf = pd.concat(coordDf)
    min_x, max_x = int(np.min(coordDf['X'])), int(np.max(coordDf['X']))
    min_y, max_y = int(np.min(coordDf['Y'])), int(np.max(coordDf['Y']))
    coordBound = [min_x, min_y, max_x, max_y] #find the gcode bounding box
    return coord, coordDf, coordBound

def loadnordsoncoord(file):
    coordDf = pd.read_csv(file)
    coords = coordDf.to_numpy().tolist()
    coords.pop(0)
    min_x, max_x = int(np.min(coordDf['X'])), int(np.max(coordDf['X']))
    min_y, max_y = int(np.min(coordDf['Y'])), int(np.max(coordDf['Y']))
    coordBound = [min_x, min_y, max_x, max_y]
    return coords, coordDf, coordBound

# This function plots the segmented gcode in a webconsole
def slider(df, all_layers):
    # 2D line plot
    # fig = px.line(df, x="X", y="Y", color = 'Step')

    # slider to select z level
    if all_layers:
        fig = px.line(
            df,
            x="X",
            y="Y",
            animation_frame="Z",
            color="Step"
        )

        steps = []
        fig["layout"].pop("updatemenus") # optional, drop animation buttons
    else:
        # 3D line plot
        fig = px.line_3d(df, x="X", y="Y", z="Z", color='Step')
    fig.show()


def select_points_gcode(action, x, y, flags, *userdata):
    global points_gcode
    # Mark a dot, when left mouse button is pressed
    if action == cv2.EVENT_LBUTTONDOWN:
        points_gcode.append([x, y])

def select_points_image(action, x, y, flags, *userdata):
    global points_img
    # Mark a dot, when left mouse button is pressed
    if action == cv2.EVENT_LBUTTONDOWN:
        points_img.append([x, y])

def select_points_cam(action, x, y, flags, *userdata):
    global points_cam
    # Mark a dot, when left mouse button is pressed
    if action == cv2.EVENT_LBUTTONDOWN:
        points_cam.append([x, y])

def show_distance(event, x, y, args, params):
    global points_distance
    points_distance = (x, y)


def gcode_image_align(gcode, img, size, linewidth, pixpermm):
    global points_gcode, points_img
    gcode_image = gcode_to_image(gcode, size, linewidth, pixpermm)
    cv2.namedWindow("Alignment", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO) # dragable windows
    cv2.namedWindow("Image", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO) # dragable windows
    cv2.setMouseCallback('Alignment', select_points_gcode)
    cv2.setMouseCallback('Image', select_points_image)
    while True:
        frame_g = gcode_image.copy()
        frame_i = img.copy()
        for i in range(0, len(points_gcode)):
            cv2.circle(frame_g, points_gcode[i], 10, (255, 0, 0), -1)
            cv2.putText(frame_g, str(i+1), (points_gcode[i][0]+10, points_gcode[i][1]+10), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0), 3)

        for n in range(0, len(points_img)):
            cv2.circle(frame_i, points_img[n], 10, (255, 0, 0), -1)
            cv2.putText(frame_i, str(n+1), (points_img[n][0]+10, points_img[n][1]+10), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 255, 0), 3)

        cv2.imshow("Alignment", frame_g)
        cv2.imshow("Image", frame_i)
        k = cv2.waitKeyEx(1)

        if k == ord('r') or k == 8:
            points_gcode = []
            points_img = []
        if k == 27 or k == ord('q') or k == ord(' '):
            cv2.destroyAllWindows()
            return points_gcode, points_img


# This function takes the segmentgcode 'coord' and renders a mask based on the
# live_cam_calibration outputs
def gcode_to_mask(coords, image, offset, cropWindow, width, scale, n_layer,
                  points_cam):
    temp_z = []
    mask = np.zeros(image.shape, np.uint8)
    for segment in coords: # plot lines per segment
        z = [item[2] for item in segment]
        temp_z.append(np.unique(z))
    layer_z = np.unique(temp_z)
    if n_layer == 'all':
        for segment in coords:  # plot lines per segment
            x_gcode = [item[0] for item in segment]
            y_gcode = [item[1] for item in segment]
            y_gcode_invert = [0 - n for n in y_gcode]
            z = [item[2] for item in segment]
            for i in range(0, len(x_gcode) - 1):
                x1, y1 = int((x_gcode[i] - offset[0]) / scale + points_cam[0][0]), int(
                    (y_gcode_invert[i] + offset[1]) / scale + points_cam[0][1])
                x2, y2 = int((x_gcode[i + 1] - offset[0]) / scale + points_cam[0][0]), int(
                    (y_gcode_invert[i + 1] + offset[1]) / scale + points_cam[0][1])
                line_width = int(width / scale)
                cv2.line(mask, (x1, y1), (x2, y2), (0, 0, 255), line_width)
    else:
        for segment in coords:  # plot lines per segment
            x_gcode = [item[0] for item in segment]
            y_gcode = [item[1] for item in segment]
            y_gcode_invert = [0 - n for n in y_gcode]
            z = [item[2] for item in segment]
            if z[0] == layer_z[n_layer]:
                for i in range(0, len(x_gcode) - 1):
                    x1, y1 = int((x_gcode[i] - offset[0]) / scale + points_cam[0][0]), int(
                        (y_gcode_invert[i] + offset[1]) / scale + points_cam[0][1])
                    x2, y2 = int((x_gcode[i + 1] - offset[0]) / scale + points_cam[0][0]), int(
                        (y_gcode_invert[i + 1] + offset[1]) / scale + points_cam[0][1])
                    line_width = int(width / scale)
                    cv2.line(mask, (x1, y1), (x2, y2), (255, 255, 255), line_width)
    cropMask = mask[cropWindow[0]:cropWindow[1],
                  cropWindow[2]:cropWindow[3]]
    #cv2.imwrite('Test_mask.png', cropMask)
    cv2.imshow('cropped mask', cropMask)
    cv2.waitKey(0)
    return cropMask


# This function calls a live camera You can then specify a [0,0] and calibrate the frame with known
# distance. Then, the G-code rendered layer will be displayed on top of the live frames
def callcam(coord, offset, ref, width, camsource = 0, file = ""):
    global points_distance, points_cam
    cam = cv2.VideoCapture(camsource)
    cv2.namedWindow("Frame", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO) # dragable windows
    cv2.setMouseCallback('Frame', select_points_cam)
    scale = 0

    ## collect all layer heights from G-code
    temp_z = []
    for segment in coord: # plot lines per segment
        z = [item[2] for item in segment]
        temp_z.append(np.unique(z))
    layer_z = np.unique(temp_z)

    while True:
        #ret_val, frame = cam.read()
        frame = cv2.imread(file)
        #frame = cv2.flip(frame, 1)

        if len(points_cam) > 0:
            for i in range(0, len(points_cam)):
                cv2.circle(frame, points_cam[i], 3, (255, 0, 0), -1)

        if len(points_cam) == 2:
            scale = ref / abs(points_cam[0][0] - points_cam[1][0])

        if scale > 0:
            cv2.setMouseCallback('Frame', show_distance)
            if len(points_distance) > 0:
                x, y = (points_distance[0]-points_cam[0][0])*scale+offset[0], (points_distance[1]-points_cam[0][1])*scale+offset[1]
                x, y = np.round(x, 2), np.round(y, 2)
                cv2.circle(frame, points_distance, 3, (0, 0, 255))
                cv2.putText(frame, str(x) + ", "+ str(y), (points_distance[0], points_distance[1] - 20), cv2.FONT_HERSHEY_PLAIN, 2, (0, 0, 0), 2)
                for segment in coord:  # plot lines per segment
                    x_gcode = [item[0] for item in segment]
                    y_gcode = [item[1] for item in segment]
                    y_gcode_invert = [0 - n for n in y_gcode]
                    z = [item[2] for item in segment]
                    for i in range(0, len(x_gcode) - 1):
                        x1, y1 = int((x_gcode[i]-offset[0])/ scale + points_cam[0][0]), int((y_gcode_invert[i] + offset[1]) / scale + points_cam[0][1])
                        x2, y2 = int((x_gcode[i + 1]-offset[0]) / scale + points_cam[0][0]), int((y_gcode_invert[i + 1] + offset[1])/ scale + points_cam[0][1])
                        line_width = int(width / scale)
                        cv2.line(frame, (x1, y1), (x2, y2), (0, 0, 255), line_width)

        cv2.imshow('Frame', frame)

        k = cv2.waitKey(1)

        if k == 27 or k == ord('q') or k == ord(' '):
            cv2.destroyAllWindows()
            cam.release()
            break


# This function takes the list output of the segmentgcode function and renders an image
# based on real distance. coord: list output, size: image size in pixels,
# linewidth: line width in mm, pixpermm: scaling factor, n_layer: specify the rendered layer
# use this to generate a reference image
def gcode_to_image(coord, coordBox, imgshape, linewidth, pixpermm, crop_mm = 0, ssp=True):
    #pixSize = [] # convert to pixel size
    #for i in size:
    #    pixSize.append(int(i/pixpermm))
    #pixSize[2] = 3 # the 3rd item should always be 3 for the RGB channel
    image = np.zeros(shape=imgshape, dtype=np.uint8)
    for segment in coord:  # plot lines per segment
        x = [item[0] for item in segment]
        y = [item[1] for item in segment]
        y_invert = [0 - n for n in y]
        for i in range(0, len(x) - 1):
            x1, y1 = int((x[i])/pixpermm), int(y_invert[i]/pixpermm+ image.shape[0])
            x2, y2 = int((x[i+1])/pixpermm), int(y_invert[i+1]/pixpermm+ image.shape[0])
            width = int(linewidth/pixpermm)
            cv2.line(image, (x1, y1), (x2, y2), (140, 217, 245), width)
    if ssp:
        for segment in coord:
            xpt = int(segment[0][0]/pixpermm)
            ypt = int((0 - segment[0][1])/pixpermm+image.shape[0])
            cv2.circle(image, (xpt, ypt), width-2, (119, 119, 237), -1)
            print(xpt,ypt)


    cropBox = [int(i/pixpermm) for i in coordBox]
    offset = int(crop_mm/pixpermm)
    image_crop = image[cropBox[1]-offset:cropBox[3]+offset, cropBox[0]-offset:cropBox[2]+offset]
    cv2.namedWindow("Gcode-mask", flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO) # dragable windows
    cv2.imshow("Gcode-mask", image_crop)
    cv2.waitKey(0)
    return image_crop




