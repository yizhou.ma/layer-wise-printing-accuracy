import numpy as np
from tkinter import filedialog
import pandas as pd
import csv
import os.path
import cv2

# Function that opens G_code, turns it into a csv file or opens the already existing one
def g_code(g_file, csv_file):
    if not os.path.isfile(csv_file):
        header = ("x", "y", "z")
        save_to_csv(g_file, csv_file, header)
    g_codes = []
    with open(csv_file) as csvfile:
        csvreader = csv.reader(csvfile, quoting=csv.QUOTE_NONNUMERIC)
        for row in csvreader[1:]:
            g_codes.append(row)
        g_lines = g_lining(g_codes)
        return g_codes, g_lines

def g_lining(g_codes):
    g_lines = []
    for i in range(1, len(g_codes)):
        g_lines.append([[g_codes[i][0], g_codes[i][1]], [g_codes[i - 1][0], g_codes[i - 1][1]]])
    return g_lines

def g_code_split(g_code):
    for line in g_code:
        print(line)
    return g_code

def save_to_csv(data, folder, header):
    save_file = filedialog.asksaveasfilename(initialdir=folder,
                                            title="Save results",
                                            defaultextension="*.csv",
                                            filetypes=[("default", "*.csv")])
    if not save_file:
        return
    else:
        dfData = pd.DataFrame(data)
        dfData.columns = header
        dfData.to_csv(save_file, index=False)

def length_line(one, two):
    A = one[0] - two[0]
    B = one[1] - two[1]
    Cs = A**2 + B**2
    if Cs > 0:
        C = np.sqrt(Cs)
    else:
        C = 1.0
    return C

def addborder(img, thickness):
    bordered = cv2.copyMakeBorder(img, thickness, thickness, thickness, thickness,
                                  cv2.BORDER_CONSTANT, value=[255, 0, 0])
    cv2.imshow('border', bordered)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return bordered


