import cv2

# Lists to store the points
top_left = []
bottom_right = []
crop = False

def draw_rectangle(action, x, y, flags, *userdata):
    global top_left, bottom_right, crop
    # Mark the top left corner, when left mouse button is pressed
    if not crop:
        if action == cv2.EVENT_LBUTTONDOWN:
            top_left = (x, y)
        # When left mouse button is released, mark bottom right corner
        elif action == cv2.EVENT_MOUSEMOVE:
            if top_left != []:
                bottom_right = (x, y)
        elif action == cv2.EVENT_LBUTTONUP:
            bottom_right = (x, y)
            crop = True


def crop_selection(frame):
    global crop, top_left, bottom_right
    top_left = []
    bottom_right = []
    crop = False
    cv2.namedWindow('Cropping', flags=cv2.WINDOW_NORMAL | cv2.WINDOW_KEEPRATIO)
    cv2.setMouseCallback('Cropping', draw_rectangle)
    while True:
        if crop:
            img_crop = frame.copy()[top_left[1]:bottom_right[1], top_left[0]:bottom_right[0]]
            img_show = img_crop.copy()
        else:
            img_crop = frame.copy()
            img_show = img_crop.copy()
            if top_left != [] and bottom_right != []:
                cv2.rectangle(img_show, top_left, bottom_right, (255, 0, 255), 1)

        cv2.imshow('Cropping', img_show)
        k = cv2.waitKeyEx(1)

        if k == ord('r') or k == 8:
            top_left = []
            bottom_right = []
            crop = False
        if k == 27 or k == ord('q') or k == ord(' '):
            cv2.destroyAllWindows()
            break
    return img_crop, top_left, bottom_right